package EvaluationManagement;

import DB.DBManager;
import EvaluationDataManagement.Evaluation;
import EvaluationDataManagement.EvaluationDAO;
import MetricDataManagement.Metric;
import UserDataManagement.SystemUser;

public class EvaluationCatalog implements EvaluationFacade {
    private EvaluationDAO evaluationDAO;
    private static EvaluationCatalog single_instance = null;

    private EvaluationCatalog() {
        evaluationDAO = new EvaluationDAO();
    }

    public static EvaluationCatalog getInstance() {
        if (single_instance == null) {
            single_instance = new EvaluationCatalog();
        }
        return single_instance;
    }

    @Override
    public boolean addEvaluation(SystemUser employee, Metric metric, SystemUser evaluator, String result) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        try {
            evaluationDAO.addEvaluation(employee, metric, evaluator, result);
            DBManager.getInstance().closeCurrentSessionwithTransaction();
            return true;
        } catch (Exception e) {
            DBManager.getInstance().closeCurrentSessionwithTransaction();
            return false;
        }
    }


    @Override
    public Object[][] showEvaluationForEmployee(SystemUser currentUser) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        Object[][] toRet =  evaluationDAO.showEvaluationForEmployee(currentUser);
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return toRet;
    }
}
