package EvaluationManagement;

import Authentication.Authentication;
import MetricDataManagement.Metric;
import UserDataManagement.SystemUser;
import Authentication.AccessLevel;

public class EvaluationCatalogProxy implements EvaluationFacade {
    private EvaluationCatalog evaluationCatalog = EvaluationCatalog.getInstance();
    private  Authentication authentication = Authentication.getInstance();

    @Override
    public boolean addEvaluation(SystemUser employee, Metric metric, SystemUser evaluator, String result) {
        if(authentication.hasPermission(AccessLevel.EvaluatorLevel))
            evaluationCatalog.addEvaluation(employee, metric, evaluator, result);
        return false;
    }

    @Override
    public Object[][] showEvaluationForEmployee(SystemUser currentUser) {
        if(authentication.hasPermission(AccessLevel.EmployeeLevel))
            evaluationCatalog.showEvaluationForEmployee(currentUser);
        return new Object[0][];
    }
}
