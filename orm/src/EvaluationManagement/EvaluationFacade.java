package EvaluationManagement;

import EvaluationDataManagement.Evaluation;
import MetricDataManagement.Metric;
import UserDataManagement.SystemUser;

public interface EvaluationFacade {
    public boolean addEvaluation(SystemUser employee, Metric metric, SystemUser evaluator, String result);
    public Object[][] showEvaluationForEmployee(SystemUser currentUser);
}
