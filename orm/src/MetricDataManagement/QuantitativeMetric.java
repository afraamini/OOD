package MetricDataManagement;

import javax.persistence.Entity;

@Entity
public class QuantitativeMetric extends MetricType {
    public QuantitativeMetric() {
        super();
        this.metricType = "کمی";
    }

    @Override
    public String getMeasure() {
        return super.getMeasure();
    }

    @Override
    public void setMeasure(String measure) {
        super.setMeasure(measure);
    }
}
