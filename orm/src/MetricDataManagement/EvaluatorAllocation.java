package MetricDataManagement;

import UserDataManagement.Evaluator;
import UserDataManagement.SystemUser;

import javax.persistence.*;

/**
 * Created by parishad on 6/12/18.
 */
@Entity
public class EvaluatorAllocation {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO, generator="my_entity_seq_gen")
    @SequenceGenerator(name="my_entity_seq_gen", sequenceName="HIBERNATE_SEQUENCE")
    private Integer idNumber;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type_id")
    private Type type;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "evaluator_id")
    private SystemUser evaluator;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public SystemUser getEvaluator() {
        return evaluator;
    }

    public void setEvaluator(SystemUser evaluator) {
        this.evaluator = evaluator;
    }
}
