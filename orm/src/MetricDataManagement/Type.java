package MetricDataManagement;

import UserDataManagement.Evaluator;
import UserDataManagement.SystemUser;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Type {

    private String name;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="my_entity_seq_gen")
    @SequenceGenerator(name="my_entity_seq_gen", sequenceName="HIBERNATE_SEQUENCE")
    private Integer idNumber;

    @OneToMany(mappedBy = "generalType")
    private Set<MetricType> metricTypes = new HashSet<>();



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addMetricType(MetricType metricType) {
        metricTypes.add(metricType);
    }

    public Integer getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(Integer idNumber) {
        this.idNumber = idNumber;
    }

    public Set<MetricType> getMetricTypes() {
        return metricTypes;
    }

    public void setMetricTypes(Set<MetricType> metricTypes) {
        this.metricTypes = metricTypes;
    }

}
