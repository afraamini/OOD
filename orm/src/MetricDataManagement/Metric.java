package MetricDataManagement;

import javax.persistence.*;

@Entity
public class Metric {

    public Integer getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(Integer idNumber) {
        this.idNumber = idNumber;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="my_entity_seq_gen")
    @SequenceGenerator(name="my_entity_seq_gen", sequenceName="HIBERNATE_SEQUENCE")
    private Integer idNumber;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "metric_type_id")
    private MetricType metricType;


    private String name;
    private String punishment;
    private String reward;

//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "metric_type_id")
//    private MetricType metricType;

    public int getIDNumber() {
        return idNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPunishment(String punishment) {
        this.punishment = punishment;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public String getName() {
        return name;
    }

    public String getPunishment() {
        return punishment;
    }

    public String getReward() {
        return reward;
    }

    public MetricType getMetricType() {
        return metricType;
    }

    public void setMetricType(MetricType metricType) {
        this.metricType = metricType;
    }


}
