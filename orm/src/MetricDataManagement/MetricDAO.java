package MetricDataManagement;

import DB.DBManager;
import UserDataManagement.Evaluator;
import UserDataManagement.JobAllocation;
import UserDataManagement.SystemUser;
import Utility.Translation;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.TypedQuery;
import java.util.ArrayList;

public class MetricDAO {
    private Session currentSession;
    private DBManager dbManager;

    public MetricDAO() {
        dbManager = DBManager.getInstance();
    }

    public void addMetric(String metricName) {
        currentSession = dbManager.getCurrentSession();
        Metric metric = new Metric();
        metric.setName(metricName);
        System.out.println(metricName);
        currentSession.persist(metric);
    }

    public Object[][] showAllMetrics() {
        currentSession = dbManager.getCurrentSession();
        ArrayList<Metric> metrics = (ArrayList<Metric>) currentSession.createQuery("From Metric").list();
        int index = 0;
        Object[][] data = new Object[metrics.size()][];
        for (Metric metric : metrics) {
            MetricType metricType = metric.getMetricType();
            String type;
            if (metricType == null)
                type = "تعریف نشده";
            else type = metric.getMetricType().getMetricType();
            data[index] = new Object[]{metric.getName(), type};
            index += 1;
        }
        return data;
    }

    public Metric findMetric(String name) {
        currentSession = dbManager.getCurrentSession();
        Metric res = (Metric) currentSession.createQuery("from Metric where name = :name").setParameter("name", name).getSingleResult();
        return res;
    }

    public void setRewardPunishment(Metric metric, String reward, String punishment) {
        currentSession = dbManager.getCurrentSession();
        metric.setReward(reward);
        metric.setPunishment(punishment);
        currentSession.update(metric);
    }

    public void setMetricType(Metric metric, String type, String measure, String interpretation) {
        currentSession = dbManager.getCurrentSession();
        Query query = currentSession.createQuery("FROM Type where name = :type");
        query.setParameter("type", type);
        Type generalType = (Type) query.getSingleResult();

        MetricType metricType;
        if (type.equals("Quan"))
            metricType = new QuantitativeMetric();
        else if (type.equals("Qual"))
            metricType = new QualitativeMetric();
        else
            metricType = new MultiMetric();
        metricType.setMeasure(measure);
        metricType.setInterpretation(interpretation);
        metricType.setGeneralType(generalType);
        currentSession.persist(metricType);

        generalType.addMetricType(metricType);
        currentSession.persist(generalType);

        query = currentSession.createQuery("update Metric set metric_type_id = :metricType" +
                " where idNumber = :id");
        query.setParameter("metricType", metricType.getIDNumber());
        query.setParameter("id", metric.getIDNumber());
        query.executeUpdate();
        System.out.println("Done");

    }

    public Object[][] showMetricTypes() {
        currentSession = dbManager.getCurrentSession();
        ArrayList<Type> types = (ArrayList<Type>) currentSession.createQuery("from Type").list();

        Object[][] data = new Object[types.size()][];
        int index = 0;
        for (Type type : types) {
            int metricTypesLen = type.getMetricTypes().size();

            Query query = currentSession.createQuery("from EvaluatorAllocation where type_id = :type_id");
            query.setParameter("type_id", type.getIdNumber());

            int evaluatorLen = query.list().size();

            data[index] = (new Object[]{Translation.translateMetricType(type.getName()), metricTypesLen, evaluatorLen});
            index += 1;
        }

        return data;
    }

    public Object[][] getUserMetricsData(Integer idNumber) {
        currentSession = dbManager.getCurrentSession();

        TypedQuery<Type> query = currentSession.createQuery("select e.type from EvaluatorAllocation e where e.evaluator.idNumber = :eval_id", Type.class);
        query.setParameter("eval_id", idNumber);

        ArrayList<Type> types = (ArrayList<Type>) query.getResultList();

        Object[][] data = new Object[types.size()][];
        int index = 0;
        for (Type type : types) {
            int metricTypesLen = type.getMetricTypes().size();

            data[index] = (new Object[]{Translation.translateMetricType(type.getName()), metricTypesLen});
            index += 1;
        }

        return data;
    }

    public Type findType(String name) {
        currentSession = dbManager.getCurrentSession();

        Type type = (Type) currentSession.createQuery("from Type where name=:name").setParameter("name", name).getSingleResult();

        return type;
    }


    public void addEvaluatorToType(SystemUser evaluator, Type type) {
        currentSession = dbManager.getCurrentSession();

        EvaluatorAllocation evaluatorAllocation = new EvaluatorAllocation();
        evaluatorAllocation.setEvaluator(evaluator);
        evaluatorAllocation.setType(type);
        currentSession.persist(evaluatorAllocation);

    }

    public void addEmployeeToType(SystemUser evaluator, Type type) {
        currentSession = dbManager.getCurrentSession();

        JobAllocation jobAllocation = new JobAllocation();
        jobAllocation.setSystemUser(evaluator);

        Evaluator evaluatorJob = (Evaluator) currentSession.createQuery("from Evaluator ").getSingleResult();
        jobAllocation.setJob(evaluatorJob);
        currentSession.persist(jobAllocation);

        EvaluatorAllocation evaluatorAllocation = new EvaluatorAllocation();
        evaluatorAllocation.setEvaluator(evaluator);
        evaluatorAllocation.setType(type);
        currentSession.persist(evaluatorAllocation);

    }

    public String getEvaluatorLen(Type type) {
        currentSession = dbManager.getCurrentSession();

        Query query = currentSession.createQuery("from EvaluatorAllocation where type_id = :type_id");
        query.setParameter("type_id", type.getIdNumber());
        String len = query.list().size() + "";

        return len;

    }

    public ArrayList<Metric> findMetrics(String targetType) {
        currentSession = dbManager.getCurrentSession();

        TypedQuery<MetricType> query = currentSession.createQuery("from MetricType m where m.generalType.name = :targetType", MetricType.class);
        query.setParameter("targetType", targetType);

        ArrayList<MetricType> metricTypes = (ArrayList<MetricType>) query.getResultList();

        TypedQuery<Metric> metricNamesQuery = currentSession.createQuery("from Metric m where m.metricType in :metricTypes", Metric.class);
        metricNamesQuery.setParameter("metricTypes", metricTypes);

        ArrayList<Metric> metrics = (ArrayList<Metric>) metricNamesQuery.getResultList(); //TODO:age meyar nadasht chi?

        return metrics;

    }

    public String findMeasureForMetric(String metricName) {
        currentSession = dbManager.getCurrentSession();

        TypedQuery<String> query = currentSession.createQuery("select m.metricType.measure from Metric m where m.name = :metricName", String.class);
        query.setParameter("metricName", metricName);

        String measure  = query.getSingleResult();

        return measure;
    }

    public String findInterpretation(String metricName) {
        currentSession = dbManager.getCurrentSession();

        TypedQuery<String> query = currentSession.createQuery("select m.metricType.interpretation from Metric m where m.name = :metricName", String.class);
        query.setParameter("metricName", metricName);

        String interpretation  = query.getSingleResult();

        return interpretation;

    }

    public ArrayList<String> findMeasureInterpretation(String metricName) {
        currentSession = dbManager.getCurrentSession();

        ArrayList<String> toReturn = new ArrayList<>();

        TypedQuery<Integer> query = currentSession.createQuery("select m.metricType.idNumber from Metric m where m.name = :metricName", Integer.class);
        query.setParameter("metricName", metricName);

        int multiID  = query.getSingleResult();
        TypedQuery<MultiMetric> multiMetricQuery = currentSession.createQuery("from MultiMetric m where m.idNumber = :ID", MultiMetric.class);
        multiMetricQuery.setParameter("ID", multiID);
        MultiMetric multiMetric = multiMetricQuery.getSingleResult();

        toReturn.add(multiMetric.getQualInterpretation());
        toReturn.add(multiMetric.getQuanInterpretation());
        toReturn.add(multiMetric.getQualMeasure());
        toReturn.add(multiMetric.getQuanMeasure());

        return toReturn;

    }
}
