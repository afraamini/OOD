package MetricDataManagement;

import javax.persistence.Entity;

@Entity
public class QualitativeMetric extends MetricType {
    public QualitativeMetric() {
        super();
        metricType = "کیفی";
    }

    @Override
    public String getMeasure() {
        return super.getMeasure();
    }

    @Override
    public void setMeasure(String measure) {
        super.setMeasure(measure);
    }
}
