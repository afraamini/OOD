package MetricDataManagement;

import javax.persistence.Entity;

@Entity
public class MultiMetric extends MetricType {
    public MultiMetric() {
        super();
        this.metricType = "کمی کیفی";
    }


    public String getQualInterpretation(){
        String[] interpretations=super.getInterpretation().split("\n");
        return interpretations[1];
    }

    public String getQuanInterpretation(){
        String[] interpretations=super.getInterpretation().split("\n");
        return interpretations[3];
    }

    public String getQualMeasure(){
        String[] measures = super.getMeasure().split("\n");
        return measures[1];
    }

    public String getQuanMeasure(){
        String[] measures = super.getMeasure().split("\n");
        return measures[3];
    }
}
