package MetricDataManagement;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class MetricType {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="my_entity_seq_gen")
    @SequenceGenerator(name="my_entity_seq_gen", sequenceName="HIBERNATE_SEQUENCE")
    private Integer idNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type_id")
    private Type generalType;

    protected String measure;
    protected String metricType;

    protected String interpretation;

    public String getInterpretation() {
        return interpretation;
    }

    public void setInterpretation(String interpretation) {
        this.interpretation = interpretation;
    }


    public int getIDNumber() {
        return idNumber;
    }


    public void setIdNumber(Integer idNumber) {
        this.idNumber = idNumber;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getMetricType() {
        return metricType;
    }

    public void setMetricType(String metricType) {
        this.metricType = metricType;
    }

    public Type getGeneralType() {
        return generalType;
    }

    public void setGeneralType(Type generalType) {
        this.generalType = generalType;
    }

    @Override
    public String toString(){
        return getGeneralType().getName();
    }
}
