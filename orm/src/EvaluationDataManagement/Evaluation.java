package EvaluationDataManagement;

import MetricDataManagement.Metric;
import UserDataManagement.SystemUser;

import javax.persistence.*;

@Entity
public class Evaluation {
    public Integer getIdNumber() {
        return idNumber;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="my_entity_seq_gen")
    @SequenceGenerator(name="my_entity_seq_gen", sequenceName="HIBERNATE_SEQUENCE")
    protected Integer idNumber;
    protected String result;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    private SystemUser employee;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "evaluator_id")
    private SystemUser evaluator;


    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "metric_id")
    private Metric metric;

    public SystemUser getEvaluator() {
        return evaluator;
    }

    public void setEvaluator(SystemUser evaluator) {
        this.evaluator = evaluator;
    }


    public SystemUser getEmployee() {
        return employee;
    }

    public void setEmployee(SystemUser employee) {
        this.employee = employee;
    }


    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

}
