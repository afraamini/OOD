package EvaluationDataManagement;

import DB.DBManager;
import MetricDataManagement.Metric;
import UserDataManagement.SystemUser;
import Utility.Translation;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Entity;
import java.util.ArrayList;

@Entity
public class EvaluationDAO {
    private Session currentSession;
    private DBManager dbManager;

    public EvaluationDAO() {
        dbManager = DBManager.getInstance();
    }

    public boolean addEvaluation(SystemUser employee, Metric metric, SystemUser evaluator, String result) {
        currentSession = dbManager.getCurrentSession();

        Evaluation evaluation = new Evaluation();
        evaluation.setEmployee(employee);
        evaluation.setEvaluator(evaluator);
        evaluation.setMetric(metric);
        evaluation.setResult(result);

        currentSession.persist(evaluation);
        return true;
    }
    public Object[][] showEvaluationForEmployee(SystemUser currUser){
        currentSession = dbManager.getCurrentSession();
        if(currentSession.createQuery("From Evaluation where employee_id = :employee_id").setParameter("employee_id", currUser.getIdNumber()).getResultList().isEmpty())
        {
            return new Object[0][];
        }
        ArrayList<Evaluation> evaluations = (ArrayList<Evaluation>) currentSession.createQuery("From Evaluation where employee_id = :employee_id").setParameter("employee_id", currUser.getIdNumber()).list();
        int index = 0;
        Object[][] data = new Object[evaluations.size()][];
        for (Evaluation evaluation : evaluations) {
            data[index] = new Object[]{evaluation.getMetric().getName(), Translation.translateMetricType(evaluation.getMetric().getMetricType().toString()),evaluation.getResult()};
            index += 1;
        }
        return data;
    }
}
