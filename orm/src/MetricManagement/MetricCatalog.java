package MetricManagement;

import DB.DBManager;
import MetricDataManagement.Metric;
import MetricDataManagement.MetricDAO;
import MetricDataManagement.Type;
import UserDataManagement.SystemUser;

import java.util.ArrayList;

public class MetricCatalog implements MetricFacade {

    private MetricDAO metricDAO;
    private static MetricCatalog single_instance = null;

    private MetricCatalog() {
        metricDAO = new MetricDAO();

    }

    public static MetricCatalog getInstance() {
        if (single_instance == null)
            single_instance = new MetricCatalog();

        return single_instance;

    }

    @Override
    public boolean addMetric(String metricName) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        try {
            metricDAO.addMetric(metricName);
            DBManager.getInstance().closeCurrentSessionwithTransaction();

            return true;
        } catch (Exception e) {
            DBManager.getInstance().closeCurrentSessionwithTransaction();
            return false;
        }

    }

    @Override
    public Object[][] showAllMetrics() {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        Object[][] toRet = metricDAO.showAllMetrics();
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return toRet;

    }

    @Override
    public Metric findMetric(String metricName) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        Metric toRet = metricDAO.findMetric(metricName);
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return toRet;
    }

    @Override
    public boolean setMetricType(Metric metric, String type, String measure, String interpretation) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        try {
            metricDAO.setMetricType(metric, type, measure, interpretation);
            DBManager.getInstance().closeCurrentSessionwithTransaction();
            return true;
        } catch (Exception e) {
            DBManager.getInstance().closeCurrentSessionwithTransaction();
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean setRewardPunishment(Metric metric, String reward, String punishment) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        try {
            metricDAO.setRewardPunishment(metric, reward, punishment);
            DBManager.getInstance().closeCurrentSessionwithTransaction();
            return true;
        } catch (Exception e) {
            DBManager.getInstance().closeCurrentSessionwithTransaction();
            return false;
        }
    }

    @Override
    public Object[][] showMetricTypes() {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        Object[][] toRet = metricDAO.showMetricTypes();
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return toRet;
    }

    @Override
    public Type findType(String name) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        Type toRet = metricDAO.findType(name);
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return toRet;
    }

    @Override
    public String showMetricTypes(String type) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        ArrayList<Metric> metrics = metricDAO.findMetrics(type);
        String toShowMetrics = "";
        for (Metric metric : metrics) {
            toShowMetrics += "\n" + metric.getName();
        }

        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return toShowMetrics;
    }

    @Override
    public boolean addEvaluatorToType(SystemUser evaluator, Type type) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        try {
            metricDAO.addEvaluatorToType(evaluator, type);
            DBManager.getInstance().closeCurrentSessionwithTransaction();
            return true;
        } catch (Exception e) {
            DBManager.getInstance().closeCurrentSessionwithTransaction();
            return false;
        }
    }

    @Override
    public boolean addEmployeeToType(SystemUser evaluator, Type type) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        try {
            metricDAO.addEmployeeToType(evaluator, type);
            DBManager.getInstance().closeCurrentSessionwithTransaction();
            return true;
        } catch (Exception e) {
            DBManager.getInstance().closeCurrentSessionwithTransaction();
            return false;
        }
    }

    @Override
    public String getEvaluatorLen(Type type) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        String toRet = metricDAO.getEvaluatorLen(type);
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return toRet;
    }

    @Override
    public Object[][] getUserMetricsData(Integer evaluatorID) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        Object[][] toRet = metricDAO.getUserMetricsData(evaluatorID);
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return toRet;
    }

    @Override
    public ArrayList<String> findMetrics(String targetType) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        ArrayList<Metric> metrics = metricDAO.findMetrics(targetType);
        ArrayList<String> toReturn = new ArrayList<>();
        toReturn.add(Utility.Translation.translateMetricType(targetType));

        DBManager.getInstance().openCurrentSessionwithTransaction();
        String metricNames = "";

        for (Metric metric : metrics) {
            metricNames = metricNames.concat((metric.getName() + "\n"));
        }
        toReturn.add(metricNames);
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return toReturn;
    }

    @Override
    public String findMeasureForMetric(String metricName) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        String toRet = metricDAO.findMeasureForMetric(metricName);
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return toRet;

    }

    @Override
    public String findInterpretation(String metricName) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        String toRet = metricDAO.findInterpretation(metricName);
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return toRet;
    }

    @Override
    public ArrayList<String> findMeasureInterpretation(String metricName) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        ArrayList<String> toRet = metricDAO.findMeasureInterpretation(metricName);
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return toRet;
    }
}
