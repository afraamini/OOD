package MetricManagement;

import MetricDataManagement.Metric;
import MetricDataManagement.MetricType;
import MetricDataManagement.Type;
import UserDataManagement.SystemUser;

import java.util.ArrayList;
import java.util.List;

public interface MetricFacade {
    public boolean addMetric(String metricName);
    public boolean setMetricType(Metric metric, String type, String measure, String interpretation);
    public boolean setRewardPunishment(Metric metric, String reward, String punishment);
    public Object[][] showAllMetrics();
    public Object[][] showMetricTypes();
    public Type findType(String name);
    public String showMetricTypes(String type);
    public boolean addEvaluatorToType(SystemUser evaluator, Type type);
    public String getEvaluatorLen(Type type);
    public boolean addEmployeeToType(SystemUser evaluator, Type type);
    public Object[][] getUserMetricsData(Integer evaluatorID);
    public ArrayList<String> findMetrics(String targetType);
    public Metric findMetric(String metricName);
    public String findMeasureForMetric(String metricName);
    public String findInterpretation(String metricName);
    public ArrayList<String> findMeasureInterpretation(String metricName);
}
