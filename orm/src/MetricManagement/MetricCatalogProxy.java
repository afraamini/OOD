package MetricManagement;

import Authentication.Authentication;
import MetricDataManagement.Metric;
import MetricDataManagement.Type;
import UserDataManagement.SystemUser;
import Authentication.AccessLevel;

import java.util.ArrayList;

public class MetricCatalogProxy implements MetricFacade {
    private MetricCatalog metricCatalog = MetricCatalog.getInstance();
    private Authentication authentication = Authentication.getInstance();
    @Override
    public boolean addMetric(String metricName) {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            return metricCatalog.addMetric(metricName);
        return false;
    }

    @Override
    public boolean setMetricType(Metric metric, String type, String measure, String interpretation) {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            return metricCatalog.setMetricType(metric, type, measure, interpretation);
        return false;
    }

    @Override
    public boolean setRewardPunishment(Metric metric, String reward, String punishment) {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            return metricCatalog.setRewardPunishment(metric, reward, punishment);
        return false;
    }

    @Override
    public Object[][] showAllMetrics() {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            return metricCatalog.showAllMetrics();
        return new Object[0][];
    }

    @Override
    public Object[][] showMetricTypes() {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            return metricCatalog.showMetricTypes();
        return new Object[0][];
    }

    @Override
    public Type findType(String name) {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            return metricCatalog.findType(name);
        return null;
    }

    @Override
    public String showMetricTypes(String type) {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            return metricCatalog.showMetricTypes(type);
        return null;
    }

    @Override
    public boolean addEvaluatorToType(SystemUser evaluator, Type type) {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            return metricCatalog.addEvaluatorToType(evaluator, type);
        return false;
    }

    @Override
    public String getEvaluatorLen(Type type) {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            return metricCatalog.getEvaluatorLen(type);
        return null;
    }

    @Override
    public boolean addEmployeeToType(SystemUser evaluator, Type type) {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            return metricCatalog.addEmployeeToType(evaluator, type);
        return false;
    }

    @Override
    public Object[][] getUserMetricsData(Integer evaluatorID) {
        if(authentication.hasPermission(AccessLevel.EvaluatorLevel) || authentication.hasPermission(AccessLevel.managerLevel))
            return metricCatalog.getUserMetricsData(evaluatorID);
        return new Object[0][];
    }

    @Override
    public ArrayList<String> findMetrics(String targetType) {
        if(authentication.hasPermission(AccessLevel.EvaluatorLevel) || authentication.hasPermission(AccessLevel.managerLevel))
            return metricCatalog.findMetrics(targetType);
        return null;
    }

    @Override
    public Metric findMetric(String metricName) {
        if(authentication.hasPermission(AccessLevel.EvaluatorLevel) || authentication.hasPermission(AccessLevel.managerLevel))
            return metricCatalog.findMetric(metricName);
        return null;
    }

    @Override
    public String findMeasureForMetric(String metricName) {
        if(authentication.hasPermission(AccessLevel.EvaluatorLevel))
            return metricCatalog.findMeasureForMetric(metricName);
        return null;
    }

    @Override
    public String findInterpretation(String metricName) {
        if(authentication.hasPermission(AccessLevel.EvaluatorLevel))
            return metricCatalog.findInterpretation(metricName);
        return null;
    }

    @Override
    public ArrayList<String> findMeasureInterpretation(String metricName) {
        if(authentication.hasPermission(AccessLevel.EvaluatorLevel))
            return metricCatalog.findMeasureInterpretation(metricName);
        return null;
    }
}
