package Authentication;

import DB.DBManager;
import UI.MainDialog;
import UI.MessageType;
import UserDataManagement.SystemUser;
import UserDataManagement.UserAuthDAO;
import org.hibernate.Session;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;

public class Authentication {
    private SystemUser currentUser;
    private UserAuthDAO userAuthDAO;

    private static Authentication single_instance = null;

    private Authentication() {
        userAuthDAO = new UserAuthDAO();

    }

    public static Authentication getInstance() {
        if (single_instance == null)
            single_instance = new Authentication();

        return single_instance;
    }

    public void logout() {
        this.currentUser = null;
        new MainDialog(MessageType.SUCCESS, "خروج از حساب کاربری");
    }

    public String login(String personnelID, String password) {
        String role = "";
        Session session = DBManager.getInstance().openCurrentSessionwithTransaction();
        currentUser = userAuthDAO.login(personnelID, password);
        if (currentUser == null) {
            new MainDialog(MessageType.NOT_COMPLETE, "شماره پرسنلی یا رمز عبور اشتباه است");
            role = "None";
        } else {
            //check if current user is a manager
            ArrayList<Integer> managerIDs = (ArrayList<Integer>) session.createQuery("select idNumber from Manager").getResultList();
            Query query = session.createQuery("from JobAllocation where job_id in :managerIDs and user_id = :currentID");
            query.setParameter("managerIDs", managerIDs);
            query.setParameter("currentID", currentUser.getIdNumber());
            try {
                query.getSingleResult();
                if (((org.hibernate.query.Query) query).list().size() != 0) role = "Manager";
            } catch (javax.persistence.NoResultException e) {
                //check if current user is an evaluator
                ArrayList<Integer> evaluatorIDs = (ArrayList<Integer>) session.createQuery("select idNumber from Evaluator").getResultList();
                System.out.println(currentUser.getIdNumber());
                query = session.createQuery("from JobAllocation where job_id in :evaluatorIDs and user_id = :currentID");
                query.setParameter("evaluatorIDs", evaluatorIDs);
                query.setParameter("currentID", currentUser.getIdNumber());
                try {
                    query.getSingleResult();
                    if (((org.hibernate.query.Query) query).list().size() != 0) role = "Evaluator";//TODO in bara man error mide vaghti ke jadavl evaluator khalie
                } catch (javax.persistence.NoResultException e2) {
                    role = "Employee";
                }
            }
//                catch (Exception e) {
//                    role = "None";
//                    new MainDialog(MessageType.NOT_COMPLETE, "واکشی دچار خطا شده است. دوباره تلاش کنید.");
//                    System.out.println(e.getMessage());
//                }
        }
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return role;
    }

    public SystemUser getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(SystemUser currentUser) {
        this.currentUser = currentUser;
    }

    public boolean hasPermission(AccessLevel accessLevel) {
        return userAuthDAO.hasPermission(accessLevel, currentUser.getPersonelID());
    }
}
