package UserDataManagement;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
public class SystemUser {
    public Integer getIdNumber() {
        return idNumber;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="my_entity_seq_gen")
    @SequenceGenerator(name="my_entity_seq_gen", sequenceName="HIBERNATE_SEQUENCE")
    private Integer idNumber;

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String lastLogin;
    private String responsibilities;

    public void setPersonelID(String personelID) {
        this.personelID = personelID;
    }

    @Column(unique = true)
    private String personelID;

    private String unit;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPersonelID() {
        return personelID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public String getResponsibilities() {
        return responsibilities;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public void setResponsibilities(String responsibilities) {
        this.responsibilities = responsibilities;
    }

}
