package UserDataManagement;

import Authentication.AccessLevel;
import DB.DBManager;
import UserDataManagement.SystemUser;
import org.hibernate.Session;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class UserAuthDAO {

    private Session currentSession;
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();

    public SystemUser login(String personnelID, String passoword) {
        SystemUser loggedInUser;

        currentSession = DBManager.getInstance().getCurrentSession();
        Query query = currentSession.createQuery("from SystemUser where personelID = :personnelID and password = :password");
        query.setParameter("personnelID", personnelID);
        query.setParameter("password", passoword);
        try {
            loggedInUser = (SystemUser) query.getSingleResult();
            loggedInUser.setLastLogin(dtf.format(now));
            currentSession.persist(loggedInUser);
            return loggedInUser;
        }
        catch (javax.persistence.NoResultException e) {
            System.out.println("No user found! try again...");
            return null;
        }


    }

    public boolean hasPermission(AccessLevel accessLevel, String personelID) {
        DBManager dbManager = DBManager.getInstance();
        currentSession = dbManager.openCurrentSessionwithTransaction();

        TypedQuery<AccessLevel> query = currentSession.createQuery("select j.job.permissions from JobAllocation j where j.systemUser.personelID = :pID", AccessLevel.class);
        query.setParameter("pID", personelID);

        ArrayList<AccessLevel> permissions = (ArrayList<AccessLevel>) query.getResultList();
        for (AccessLevel permission : permissions) {
            if(accessLevel.equals(permission))  {
                dbManager.closeCurrentSessionwithTransaction();
                return true;
            }

        }
        dbManager.closeCurrentSessionwithTransaction();
        return false;
    }
}
