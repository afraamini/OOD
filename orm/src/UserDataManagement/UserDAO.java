package UserDataManagement;

import DB.DBManager;
import MetricDataManagement.EvaluatorAllocation;
import MetricDataManagement.Type;
import UserManagement.IUserDao;
import org.hibernate.Session;

import javax.persistence.Query;
import java.util.ArrayList;

/**
 * Created by parishad on 6/12/18.
 */
public class UserDAO implements IUserDao {

    private Session currentSession;
    private DBManager dbManager;

    public UserDAO() {
        dbManager = DBManager.getInstance();
    }

    public boolean addManager(String firstName, String lastName, String email, String personelID, String responsibilities, String unit, String password) {
        currentSession = dbManager.getCurrentSession();
        if (!currentSession.createQuery("from SystemUser where personelID = :personelID").setString("personelID", personelID).getResultList().isEmpty()) {
            return false;
        }

        SystemUser employee = new SystemUser();
        employee.setEmail(email);
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setPassword(password);
        employee.setPersonelID(personelID);
        employee.setResponsibilities(responsibilities);
        employee.setUnit(unit);
        currentSession.persist(employee);

        Job employeeJob = new Manager();
        currentSession.persist(employeeJob);

        JobAllocation jobAllocation = new JobAllocation();
        jobAllocation.setJob(employeeJob);
        jobAllocation.setSystemUser(employee);
        currentSession.persist(jobAllocation);
        return true;
    }

    public boolean addEmployee(String firstName, String lastName, String email, String personelID, String responsibilities, String unit, String password) {
        currentSession = dbManager.getCurrentSession();
        if (!currentSession.createQuery("from SystemUser where personelID = :personelID").setString("personelID", personelID).getResultList().isEmpty()) {
            return false;
        }

        SystemUser employee = new SystemUser();
        employee.setEmail(email);
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setPassword(password);
        employee.setPersonelID(personelID);
        employee.setResponsibilities(responsibilities);
        employee.setUnit(unit);
        currentSession.persist(employee);

        Job employeeJob = new Employee();
        currentSession.persist(employeeJob);

        JobAllocation jobAllocation = new JobAllocation();
        jobAllocation.setJob(employeeJob);
        jobAllocation.setSystemUser(employee);
        currentSession.persist(jobAllocation);
        return true;
    }

    @Override
    public ArrayList<SystemUser> showAllEvaluators() {
        currentSession = dbManager.getCurrentSession();

        Query query = currentSession.createQuery("from Evaluator");
        ArrayList<Job> jobs = (ArrayList<Job>) ((org.hibernate.query.Query) query).list();

        ArrayList<Integer> jobIDs = new ArrayList<>();
        for (Job job : jobs) {
            jobIDs.add(job.idNumber);
        }

        query = currentSession.createQuery("from JobAllocation where job_id = :ids");
        query.setParameter("ids", jobIDs);
        ArrayList<SystemUser> evaluators = new ArrayList<>();

        ArrayList<JobAllocation> jobsAll = (ArrayList<JobAllocation>) ((org.hibernate.query.Query) query).list();
        for (JobAllocation jobAll : jobsAll) {
            evaluators.add(jobAll.getSystemUser());
        }
        return evaluators;
    }

    public ArrayList<SystemUser> showAllEmployees() { //not used
        currentSession = dbManager.getCurrentSession();

        Query query = currentSession.createQuery("from Employee");
        ArrayList<Job> jobs = (ArrayList<Job>) ((org.hibernate.query.Query) query).list();

        ArrayList<Integer> jobIDs = new ArrayList<>();
        for (Job job : jobs) {
            jobIDs.add(job.idNumber);
        }

        query = currentSession.createQuery("from JobAllocation where job_id in :ids");
        query.setParameter("ids", jobIDs);

        ArrayList<JobAllocation> jobsAll = (ArrayList<JobAllocation>) ((org.hibernate.query.Query) query).list();
        ArrayList<SystemUser> employees = new ArrayList<>();

        for (JobAllocation jobAll : jobsAll) {
            employees.add(jobAll.getSystemUser());
        }
        return employees;
    }


    public SystemUser findEmployee(String PersonelID) {
        currentSession = dbManager.getCurrentSession();
        SystemUser user = (SystemUser) currentSession.createQuery("from SystemUser where personelID = :personelID").setString("personelID", PersonelID).getSingleResult();
        return user;
    }

    public ArrayList<Type> getTypesForEvaluator(String PersonelID) {
        currentSession = dbManager.getCurrentSession();
        SystemUser user = (SystemUser) currentSession.createQuery("from SystemUser where personelID = :personelID").setParameter("personelID", PersonelID).getSingleResult();

        ArrayList<EvaluatorAllocation> evalAlloc = (ArrayList<EvaluatorAllocation>) currentSession.createQuery("from EvaluatorAllocation  where evaluator_id = :evaluator_id").setParameter("evaluator_id", user.getIdNumber()).list();
        ArrayList<Type> types = new ArrayList<>();

        for (EvaluatorAllocation evaluatorAllocation : evalAlloc) {
            types.add(evaluatorAllocation.getType());
        }

        return types;
    }
}
