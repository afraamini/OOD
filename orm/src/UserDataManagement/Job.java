package UserDataManagement;

import Authentication.AccessLevel;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Job {
    public Integer getIdNumber() {
        return idNumber;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="my_entity_seq_gen")
    @SequenceGenerator(name="my_entity_seq_gen", sequenceName="HIBERNATE_SEQUENCE")
    protected Integer idNumber;

    protected AccessLevel permissions;

    public AccessLevel getPermissions() {
        return permissions;
    }

    public void setPermissions(AccessLevel permissions) {
        this.permissions = permissions;
    }
}

