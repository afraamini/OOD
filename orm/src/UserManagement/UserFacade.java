package UserManagement;

import MetricDataManagement.Type;
import UserDataManagement.SystemUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by parishad on 6/12/18.
 */
public interface UserFacade {
    public ArrayList<SystemUser> showAllEvaluators();
    public Object[][] showAllEmployeesData(String currentUserPersonellID);
    public SystemUser getEmployee(String PersonelID);
    public Object[][] showAllNonEvaluators();
    public Object[][] showAllEvaluatorsExceptType(int typeId);
    public void updateEvaluators();
    public void updateEmployees();
    public boolean addEmployee(String firstName, String lastName, String email, String personelID,
                               String responsibilities, String unit, String password);
    public boolean addManager(String firstName, String lastName, String email, String personelID, String responsiblities, String unit, String password);
}
