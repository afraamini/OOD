package UserManagement;

import UserDataManagement.SystemUser;
import UserDataManagement.UserDAO;

import java.util.ArrayList;

public class UserProxy implements IUserDao {
    private UserDAO userDAO;

    private boolean updateEvaluator;

    private boolean updateEmployee;
    private ArrayList<SystemUser> evaluators;
    private ArrayList<SystemUser> employees;
    public UserProxy(UserDAO userDAO){
        updateEmployee=true;
        this.userDAO = userDAO;
    }

    public void setUpdateEvaluator(boolean updateEvaluator) {
        this.updateEvaluator = updateEvaluator;
    }

    public void setUpdateEmployee(boolean updateEmployee) {
        this.updateEmployee = updateEmployee;
    }

    @Override
    public ArrayList<SystemUser> showAllEvaluators() {
        if(evaluators==null || updateEvaluator){
            updateEvaluator = false;
            evaluators = userDAO.showAllEvaluators();
        }
        return evaluators;
    }

    @Override
    public ArrayList<SystemUser> showAllEmployees() {
        if(employees==null || updateEmployee){
            updateEmployee = false;
            employees = userDAO.showAllEmployees();
        }
        return employees;
    }


}
