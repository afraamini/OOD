package UserManagement;

import Authentication.Authentication;
import MetricDataManagement.Type;
import UserDataManagement.SystemUser;
import Authentication.AccessLevel;

import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.List;

public class UserCatalogProxy implements UserFacade {
    private UserCatalog userCatalog = UserCatalog.getInstance();
    private Authentication authentication = Authentication.getInstance();

    @Override
    public ArrayList<SystemUser> showAllEvaluators() {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            return userCatalog.showAllEvaluators();
        return null;
    }

    @Override
    public Object[][] showAllEmployeesData(String currentUserPersonellID) {
        if(authentication.hasPermission(AccessLevel.managerLevel) || authentication.hasPermission(AccessLevel.EvaluatorLevel))
            return userCatalog.showAllEmployeesData(currentUserPersonellID);
            return new Object[0][];
    }

    @Override
    public SystemUser getEmployee(String PersonelID) {
        if(authentication.hasPermission(AccessLevel.managerLevel) || authentication.hasPermission(AccessLevel.EvaluatorLevel))
            return userCatalog.getEmployee(PersonelID);
        return null;
    }

    @Override
    public Object[][] showAllNonEvaluators() {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            return userCatalog.showAllNonEvaluators();
            return new Object[0][];
    }

    @Override
    public Object[][] showAllEvaluatorsExceptType(int typeId) {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            return userCatalog.showAllEvaluatorsExceptType(typeId);
            return new Object[0][];
    }

    @Override
    public void updateEvaluators() {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            userCatalog.updateEvaluators();

    }

    @Override
    public void updateEmployees() {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            userCatalog.updateEmployees();

    }

    @Override
    public boolean addEmployee(String firstName, String lastName, String email, String personelID, String responsibilities, String unit, String password) {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            return userCatalog.addEmployee(firstName, lastName, email, personelID, responsibilities, unit, password);
            return false;
    }

    @Override
    public boolean addManager(String firstName, String lastName, String email, String personelID, String responsiblities, String unit, String password) {
        if(authentication.hasPermission(AccessLevel.managerLevel))
            return userCatalog.addManager(firstName, lastName, email, personelID, responsiblities, unit, password);
            return false;
    }
}
