package UserManagement;

import UserDataManagement.SystemUser;

import java.util.ArrayList;

public interface IUserDao {
    public ArrayList<SystemUser> showAllEvaluators();
    public ArrayList<SystemUser> showAllEmployees();
}
