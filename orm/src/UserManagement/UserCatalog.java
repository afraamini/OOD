package UserManagement;

import DB.DBManager;
import MetricDataManagement.Type;
import UI.MainDialog;
import UI.MessageType;
import UserDataManagement.*;

import java.util.ArrayList;

/**
 * Created by parishad on 6/12/18.
 */
public class UserCatalog implements UserFacade {
    private UserDAO userDAO;
    private UserProxy userProxy;
    private static UserCatalog single_instance = null;

    private UserCatalog() {
        userDAO = new UserDAO();
        userProxy = new UserProxy(userDAO);
    }

    public static UserCatalog getInstance() {
        if (single_instance == null)
            single_instance = new UserCatalog();

        return single_instance;

    }

    @Override
    public ArrayList<SystemUser> showAllEvaluators() {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        ArrayList<SystemUser> toRet = userProxy.showAllEvaluators();
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return toRet;
    }

    @Override
    public Object[][] showAllEvaluatorsExceptType(int typeId) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        ArrayList<SystemUser> evaluators = userProxy.showAllEvaluators();
        ArrayList<SystemUser> targetEvaluators = new ArrayList<>();

        if (evaluators == null) {
            return new Object[0][];
        }
        for (SystemUser evaluator : evaluators) {
            ArrayList<Type> types = userDAO.getTypesForEvaluator(evaluator.getPersonelID());
            ArrayList<Integer> type_ids = new ArrayList<>();
            for (Type type : types) {
                type_ids.add(type.getIdNumber());
            }
            if (!type_ids.contains(typeId)) targetEvaluators.add(evaluator);
        }

        int index = 0;
        Object[][] data = new Object[targetEvaluators.size()][];

        getEmployeeData(targetEvaluators, index, data);
//        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return data;
    }

    @Override
    public void updateEvaluators() {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        userProxy.setUpdateEvaluator(true);
        DBManager.getInstance().closeCurrentSessionwithTransaction();
    }

    @Override
    public void updateEmployees() {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        userProxy.setUpdateEmployee(true);
        DBManager.getInstance().closeCurrentSessionwithTransaction();
    }

    @Override
    public boolean addEmployee(String firstName, String lastName, String email, String personelID, String responsibilities, String unit, String password) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        try {
            if (userDAO.addEmployee(firstName, lastName, email, personelID, responsibilities, unit, password)) {
                DBManager.getInstance().closeCurrentSessionwithTransaction();
                return true;
            } else new MainDialog(MessageType.NOT_COMPLETE, "شماره پرسنلی قبلا ثبت شده است.");
        } catch (Exception e) {
            DBManager.getInstance().closeCurrentSessionwithTransaction();
        }
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return false;
    }

    @Override
    public boolean addManager(String firstName, String lastName, String email, String personelID, String responsiblities, String unit, String password) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        try {
            if (userDAO.addManager(firstName, lastName, email, personelID, responsiblities, unit, password)) {
                DBManager.getInstance().closeCurrentSessionwithTransaction();
                return true;
            } else new MainDialog(MessageType.NOT_COMPLETE, "شماره پرسنلی قبلا ثبت شده است.");
        } catch (Exception e) {
            DBManager.getInstance().closeCurrentSessionwithTransaction();
        }
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return false;
    }

    @Override
    public Object[][] showAllNonEvaluators() {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        ArrayList<SystemUser> employees = userProxy.showAllEmployees();
        ArrayList<SystemUser> evaluators = userProxy.showAllEvaluators();
        ArrayList<String> evaluators_id = new ArrayList<>();
        ArrayList<SystemUser> nonEvaluators = new ArrayList<>();
        if (evaluators == null) return new Object[0][];
        for (SystemUser evaluator : evaluators) {
            evaluators_id.add(evaluator.getPersonelID());
        }

        for (SystemUser employee : employees) {
            if (!evaluators_id.contains(employee.getPersonelID())) nonEvaluators.add(employee);
        }

        int index = 0;
        Object[][] data = new Object[nonEvaluators.size()][];

        getEmployeeData(nonEvaluators, index, data);
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return data;
    }

    private void getEmployeeData(ArrayList<SystemUser> nonEvaluators, int index, Object[][] data) {
        for (SystemUser employee : nonEvaluators) {
            String Fname = employee.getFirstName();
            String Lname = employee.getLastName();
            String id = employee.getPersonelID();

            data[index] = (new Object[]{Fname, Lname, id});
            index += 1;
        }
    }

    @Override
    public Object[][] showAllEmployeesData(String currentUserPersonellID) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        ArrayList<SystemUser> employees = userProxy.showAllEmployees();
        if (employees == null)
            return new Object[0][];
        Object[][] data = new Object[employees.size() - 1][];
        int index = 0;
        for (SystemUser employee : employees) {
            if (employee.getPersonelID().equals(currentUserPersonellID))
                continue;
            String Fname = employee.getFirstName();
            String Lname = employee.getLastName();
            String PersonelID = employee.getPersonelID();

            data[index] = (new Object[]{PersonelID, Fname, Lname});
            index += 1;
        }
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return data;
    }

    @Override
    public SystemUser getEmployee(String PersonelID) {
        DBManager.getInstance().openCurrentSessionwithTransaction();
        SystemUser toRet = userDAO.findEmployee(PersonelID);
        DBManager.getInstance().closeCurrentSessionwithTransaction();
        return toRet;
    }

}
