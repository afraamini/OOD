package Utility;

/**
 * Created by parishad on 6/12/18.
 */
public class Translation {

    public static String translateMetricType(String input) {

        switch (input) {
            case "Quan":
                return "کمی";
            case "Qual":
                return "کیفی";
            case "Multi":
                return "کمی-کیفی";
            case "کمی":
                return "Quan";
            case "کیفی":
                return "Qual";
            case "کمی-کیفی":
                return "Multi";
        }

        return null;
    }

}
