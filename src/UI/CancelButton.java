package UI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CancelButton {
    public CancelButton(JButton cnlBtn, Visibility currentPanel){

        cnlBtn.setForeground(new Color(255, 20, 147));
        cnlBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentPanel.setVisible(false);
                new ProfilePanel().setVisible(true);
            }
        });
    }
}
