package UI.EmployeeUI;

import Authentication.Authentication;
import EvaluationManagement.EvaluationCatalog;
import EvaluationManagement.EvaluationFacade;
import MetricDataManagement.Metric;
import MetricManagement.MetricFacade;
import UI.*;
import UserDataManagement.SystemUser;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

public class QualititativeEvaluation implements Visibility {
	private JPanel panel = new JPanel();
	private JButton addBtn;
	private JButton cancelBtn;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private MetricFacade metricFacade;
	private String targetType;
	private SystemUser employeeToEvaluate;
	private String metricName;
	private String[] measures;
	private String interpretation;
	private String result;
	private EvaluationFacade evaluationFacade;

	public QualititativeEvaluation(MetricFacade metricFacade, String targetType, SystemUser employeeToEvaluate, String metricName) {
		this.metricFacade = metricFacade;
		this.targetType = targetType;
		this.employeeToEvaluate = employeeToEvaluate;
		this.metricName = metricName;
		evaluationFacade = EvaluationCatalog.getInstance();
		measures = new String[3];
		prepareData();

		createComponent();
		createEvents();
	}

	private void prepareData() {
		String measure = metricFacade.findMeasureForMetric(metricName);
		interpretation = metricFacade.findInterpretation(metricName);
		measures = measure.split("-");
	}
	private void getData(){
		Enumeration elements = buttonGroup.getElements();
		while (elements.hasMoreElements()) {
			AbstractButton button = (AbstractButton)elements.nextElement();
			if (button.isSelected()) {
				result = button.getText();
			}
		}
	}
	private void createEvents() {
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getData();
				if(result==null)
					new MainDialog(MessageType.NOT_COMPLETE, "لطفا ارزیابی را انجام دهید");
				else {
					Metric metric = metricFacade.findMetric(metricName);
					if(evaluationFacade.addEvaluation(employeeToEvaluate, metric,
							Authentication.getInstance().getCurrentUser(), result))
						new MainDialog(MessageType.SUCCESS, "ارزیابی");
					else
						new MainDialog(MessageType.ERROR,"ثبت ارزیابی");

				}
			}
		});
		new CancelButton(cancelBtn,this);
	}

	private void createComponent() {
		panel = new JPanel();

		addBtn = new JButton("ثبت ارزیابی");
		addBtn.setForeground(new Color(0, 102, 0));
		
		cancelBtn = new JButton("لغو عملیات");
		cancelBtn.setForeground(UIManager.getColor("Button.select"));
		
		JLabel nameLabel = new JLabel("نام معیار");
		
		JLabel experienceLabel = new JLabel(metricName);
		
		JLabel quantWayLabel = new JLabel("نحوه تفسیر ارزیابی کیفی");
		
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setEnabled(false);
		textArea.setText(interpretation);
		
		JLabel quantLabel = new JLabel("ارزیابی کیفی");

		JRadioButton radioButton = new JRadioButton(measures[0]);
		buttonGroup.add(radioButton);

		JRadioButton radioButton_1 = new JRadioButton(measures[1]);
		buttonGroup.add(radioButton_1);

		JRadioButton radioButton_2 = new JRadioButton(measures[2]);
		buttonGroup.add(radioButton_2);
		GroupLayout groupLayout = new GroupLayout(panel);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(43)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(addBtn)
									.addPreferredGap(ComponentPlacement.RELATED, 172, Short.MAX_VALUE)
									.addComponent(cancelBtn, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
								.addComponent(quantLabel, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(quantWayLabel)
								.addComponent(nameLabel))
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(experienceLabel, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
									.addGroup(groupLayout.createSequentialGroup()
										.addComponent(radioButton)
										.addGap(39)
										.addComponent(radioButton_1)
										.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(radioButton_2))
									.addComponent(textArea, GroupLayout.PREFERRED_SIZE, 242, GroupLayout.PREFERRED_SIZE)))))
					.addGap(2147483377))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(15)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(nameLabel)
						.addComponent(experienceLabel, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(26)
							.addComponent(quantWayLabel)
							.addGap(65)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(quantLabel)
								.addComponent(radioButton_2)
								.addComponent(radioButton)
								.addComponent(radioButton_1)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addComponent(textArea, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)))
					.addGap(44)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(cancelBtn)
						.addComponent(addBtn))
					.addContainerGap(66, Short.MAX_VALUE))
		);
		
		panel.setLayout(groupLayout);
		
	}
	@Override
	public void setVisible(Boolean visible) {
		if(visible){
			MainFrame.getInstance().getFrame().getContentPane().add(panel);
			MainFrame.getInstance().getFrame().validate();
			MainFrame.getInstance().setPanel(panel);
		}else{
			MainFrame.getInstance().getFrame().getContentPane().remove(panel);
		}
	}
}
