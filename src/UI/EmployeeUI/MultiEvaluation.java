package UI.EmployeeUI;

import Authentication.Authentication;
import EvaluationManagement.EvaluationCatalog;
import EvaluationManagement.EvaluationFacade;
import MetricDataManagement.Metric;
import MetricManagement.MetricFacade;
import UI.*;
import UserDataManagement.SystemUser;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;

public class MultiEvaluation implements Visibility {
	private JPanel panel = new JPanel();
	private JButton addBtn;
	private JButton cancelBtn;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private MetricFacade metricFacade;
	private String targetType;
	private SystemUser employeeToEvaluate;
	private String metricName;
	private String[] qualMeasure = new String[3];
	private String quanMeasure;
	private String qualInterpretation;
	private String quanInterpretation;
	private JTextArea quanResultText;
	private EvaluationFacade evaluationFacade;

	public MultiEvaluation(MetricFacade metricFacade, String targetType, SystemUser employeeToEvaluate, String metricName) {
		this.metricFacade = metricFacade;
		this.targetType = targetType;
		this.employeeToEvaluate = employeeToEvaluate;
		this.metricName = metricName;
		this.evaluationFacade = EvaluationCatalog.getInstance();
		prepareData();
		createComponent();
		createEvents();
	}

	private void prepareData() {
		ArrayList<String> data = metricFacade.findMeasureInterpretation(metricName);
		qualInterpretation = data.get(0);
		quanInterpretation = data.get(1);
		qualMeasure = data.get(2).split("-");
		quanMeasure = data.get(3);
	}
	private String getData(){
		String qualResult="";
		String quanResult= quanResultText.getText();
		Enumeration elements = buttonGroup.getElements();
		while (elements.hasMoreElements()) {
			AbstractButton button = (AbstractButton)elements.nextElement();
			if (button.isSelected()) {
				qualResult = button.getText();
			}
		}
		if(qualResult==null||quanResult.equals(""))
			return "";
		return "نتیجه کمی: "+quanResult+"\n نتیجه کیفی:"+qualResult;
	}
	private void createEvents() {
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String result =getData();
				if(result.equals(""))
					new MainDialog(MessageType.NOT_COMPLETE, "لطفا ارزیابی را انجام دهید");
				else {
					Metric metric = metricFacade.findMetric(metricName);
					if(evaluationFacade.addEvaluation(employeeToEvaluate, metric,
							Authentication.getInstance().getCurrentUser(), result))
						new MainDialog(MessageType.SUCCESS, "ارزیابی");
					else
						new MainDialog(MessageType.ERROR,"ثبت ارزیابی");

				}
			}
		});
		new CancelButton(cancelBtn, this);
	}

	private void createComponent() {
		panel = new JPanel();

		addBtn = new JButton("ثبت ارزیابی");
		addBtn.setForeground(new Color(0, 102, 0));
		
		cancelBtn = new JButton("لغو عملیات");
		cancelBtn.setForeground(UIManager.getColor("Button.select"));
		
		JLabel nameLabel = new JLabel("نام معیار");
		
		JLabel experienceLabel = new JLabel(metricName);
		
		JLabel quantWayLabel = new JLabel("نحوه تفسیر ارزیابی کیفی");
		
		JLabel textArea = new JLabel();
		textArea.setText(qualInterpretation);
		
		JLabel quantLabel = new JLabel("ارزیابی کیفی");
		
		JRadioButton radioButton = new JRadioButton(qualMeasure[0]);
		buttonGroup.add(radioButton);
		
		JRadioButton radioButton_1 = new JRadioButton(qualMeasure[1]);
		buttonGroup.add(radioButton_1);
		
		JRadioButton radioButton_2 = new JRadioButton(qualMeasure[2]);
		buttonGroup.add(radioButton_2);
		
		JLabel label = new JLabel("روش ارزیابی کمی");
		JLabel textArea_1 = new JLabel();
		textArea_1.setText(quanMeasure);

		JLabel quanLabel = new JLabel("نحوه تفسیر ارزیابی کمی");
		JLabel quanIntLabel = new JLabel(quanInterpretation);

		JLabel label_1 = new JLabel("ارزیابی کمی");
		quanResultText = new JTextArea();
		GroupLayout groupLayout = new GroupLayout(panel);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(43)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(quantWayLabel)
								.addComponent(nameLabel))
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(experienceLabel, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
									.addGroup(groupLayout.createSequentialGroup()
										.addComponent(radioButton)
										.addGap(39)
										.addComponent(radioButton_1)
										.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(radioButton_2))
									.addComponent(textArea, GroupLayout.PREFERRED_SIZE, 242, GroupLayout.PREFERRED_SIZE))))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(addBtn)
									.addPreferredGap(ComponentPlacement.RELATED, 172, Short.MAX_VALUE)
									.addComponent(cancelBtn, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
								.addComponent(quantLabel, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(label, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
										.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
									.addComponent(quanLabel, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
									)
									.addGap(18)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(quanResultText, GroupLayout.PREFERRED_SIZE, 242, GroupLayout.PREFERRED_SIZE)
										.addComponent(textArea_1, GroupLayout.PREFERRED_SIZE, 242, GroupLayout.PREFERRED_SIZE)
									.addComponent(quanIntLabel, GroupLayout.PREFERRED_SIZE, 242, GroupLayout.PREFERRED_SIZE)
									)))))
					.addGap(2147483377))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(15)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(nameLabel)
						.addComponent(experienceLabel, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(26)
							.addComponent(quantWayLabel)
							.addGap(65)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(quantLabel)
								.addComponent(radioButton_2)
								.addComponent(radioButton)
								.addComponent(radioButton_1)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addComponent(textArea, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(45)
							.addComponent(label))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(29)
							.addComponent(textArea_1, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)))
					.addGap(37)
						.addGroup(groupLayout.createSequentialGroup()
						.addComponent(quanLabel)
								.addComponent(quanIntLabel, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
					.addGap(37)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1)
						.addComponent(quanResultText, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 120, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(cancelBtn)
						.addComponent(addBtn))
					.addGap(25))
		);
		
		panel.setLayout(groupLayout);
		
	}
	@Override
	public void setVisible(Boolean visible) {
		if(visible){
			MainFrame.getInstance().getFrame().getContentPane().add(panel);
			MainFrame.getInstance().getFrame().validate();
			MainFrame.getInstance().setPanel(panel);
			MainFrame.getInstance().getFrame().setMinimumSize(new Dimension(400,600));
		}else{
			MainFrame.getInstance().getFrame().getContentPane().remove(panel);
		}
	}
}
