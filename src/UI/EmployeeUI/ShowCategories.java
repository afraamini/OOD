package UI.EmployeeUI;

import Authentication.Authentication;
import MetricManagement.MetricFacade;
import UI.*;
import UserDataManagement.SystemUser;
import UserManagement.UserFacade;
//import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ShowCategories implements Visibility {

    private JPanel panel;
    private JButton chooseBtn;
    private JButton cancelBtn;
    private SystemUser employeeToEvaluate;
    private MetricFacade metricFacade;
    private String[] columnNames;
    private Object[][] data;
    private UserFacade userFacade;
    private JTable table;
    private String targetType;

    /**
     * Create the application.
     */
    public ShowCategories(SystemUser employeeToEvaluate, MetricFacade metricFacade, UserFacade userFacade) {
        super();
        this.metricFacade = metricFacade;
        this.employeeToEvaluate = employeeToEvaluate;
        this.userFacade = userFacade;
        prepareData();
        createComponents();
        createEvents();
    }

    private void prepareData() {
        columnNames = new String[]{"دسته ارزیابی",
                "تعداد معیار‌های اختصاص داده شده"};

        data = metricFacade.getUserMetricsData(Authentication.getInstance().getCurrentUser().getIdNumber());

    }
    private void createEvents() {
        chooseBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(targetType != null) {
                    ShowEvaluation showEvaluation = new ShowEvaluation(metricFacade, targetType, employeeToEvaluate);
                    MainFrame.getInstance().getFrame().setTitle("انتخاب معیار ارزیابی");
                    setVisible(false);
                    showEvaluation.setVisible(true);
                }else {
                    new MainDialog(MessageType.NOT_COMPLETE,"یک گزینه انتخاب کنید.");
                }
            }
        });

        new CancelButton(cancelBtn,this);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = table.rowAtPoint(e.getPoint());
                targetType = Utility.Translation.translateMetricType((String)data[row][0]);
                System.out.println(targetType);
            }
        });

    }

    private void createComponents() {
        panel = new JPanel();
        GroupLayout groupLayout = new GroupLayout(panel);
        panel.setLayout(groupLayout);

        chooseBtn = new JButton("انتخاب دسته معیار");
        chooseBtn.setForeground(new Color(0, 102, 0));

        cancelBtn = new JButton("لغو عملیات");
        cancelBtn.setForeground(new Color(255, 20, 147));



        JScrollPane scrollPane = new JScrollPane();
        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                        .addGroup(groupLayout.createSequentialGroup()
                                                .addGap(30)
                                                .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 482, GroupLayout.PREFERRED_SIZE))
                                        .addGroup(groupLayout.createSequentialGroup()
                                                .addGap(96)
                                                .addComponent(chooseBtn)
                                                .addGap(103)
                                                .addComponent(cancelBtn, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(32, Short.MAX_VALUE))
        );
        groupLayout.setVerticalGroup(
                groupLayout.createParallelGroup(Alignment.TRAILING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(ComponentPlacement.RELATED)
                                .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                        .addComponent(chooseBtn)
                                        .addComponent(cancelBtn))
                                .addGap(261))
        );

        table = new JTable(data, columnNames);
        scrollPane.setViewportView(table);
    }
    public JPanel getPanel(){return panel;}

    @Override
    public void setVisible(Boolean visible) {
        if(visible){
            MainFrame.getInstance().getFrame().getContentPane().add(panel);
            MainFrame.getInstance().getFrame().validate();
            MainFrame.getInstance().setPanel(panel);
            MainFrame.getInstance().getFrame().pack();
        }else{
            MainFrame.getInstance().getFrame().getContentPane().remove(panel);
        }

    }
}
