package UI.EmployeeUI;

import MetricManagement.MetricFacade;
import UI.CancelButton;
import UI.MainFrame;
import UI.Visibility;
import UserDataManagement.SystemUser;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ShowEvaluation implements Visibility {
    private JButton chooseBtn;
    private JButton btnCancel;
    private JTextField evaluationTextField;
    private JPanel panel;
    private String targetType;
    private SystemUser employeeToEvaluate;
    private MetricFacade metricFacade;
    private String catName;
    private String[] metricNames;
    private JComboBox<String> comboBox;
    /**
     * Create the application.
     */
    public ShowEvaluation(MetricFacade metricFacade, String targetType, SystemUser employeeToEvaluate) {
        this.targetType = targetType;
        this.employeeToEvaluate = employeeToEvaluate;
        this.metricFacade = metricFacade;
        prepareData();

        createComponents();
        createEvents();
    }

    private void prepareData() {
        ArrayList<String> data = metricFacade.findMetrics(targetType);
        catName = data.get(0);
        String names = data.get(1);
        metricNames = names.split("\n");
    }

    private void navigateToEvaluationPanel() {
        String metricName = (String) comboBox.getSelectedItem();
        if(targetType.equals("Qual")) {
            QualititativeEvaluation qualititativeEvaluation= new QualititativeEvaluation(metricFacade, targetType, employeeToEvaluate, metricName);
            setVisible(false);
            qualititativeEvaluation.setVisible(true);

        }else if(targetType.equals("Quan")) {
            QuantitativeEvaluation quantitativeEvaluation= new QuantitativeEvaluation(metricFacade, targetType, employeeToEvaluate, metricName);
            setVisible(false);
            quantitativeEvaluation.setVisible(true);

        }else {
            MultiEvaluation multiEvaluation= new MultiEvaluation(metricFacade, targetType, employeeToEvaluate, metricName);
            setVisible(false);
            multiEvaluation.setVisible(true);
        }
    }


    private void createEvents() {
        chooseBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                navigateToEvaluationPanel();
            }
        });
        new CancelButton(btnCancel,this);
    }

    private void createComponents() {
        panel = new JPanel();

        chooseBtn = new JButton("انتخاب معیار ارزیابی");
        chooseBtn.setForeground(new Color(0, 128, 0));

        btnCancel = new JButton("لغو عملیات");
        btnCancel.setForeground(new Color(255, 20, 147));


        JLabel nameLabel = new JLabel("نام دسته");
        nameLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 14));

        evaluationTextField = new JTextField();
        evaluationTextField.setHorizontalAlignment(SwingConstants.CENTER);
        evaluationTextField.setEditable(false);
        evaluationTextField.setEnabled(false);
        evaluationTextField.setText(catName);
        evaluationTextField.setColumns(10);

        JLabel categoryLabel = new JLabel("معیار‌های تعریف شده در دسته");
        categoryLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 14));
        
        comboBox = new JComboBox<>(metricNames);

        GroupLayout groupLayout = new GroupLayout(panel);
        groupLayout.setHorizontalGroup(
        	groupLayout.createParallelGroup(Alignment.TRAILING)
        		.addGroup(groupLayout.createSequentialGroup()
        			.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
        				.addGroup(groupLayout.createSequentialGroup()
        					.addGap(34)
        					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
        						.addComponent(categoryLabel, GroupLayout.PREFERRED_SIZE, 157, GroupLayout.PREFERRED_SIZE)
        						.addComponent(nameLabel)))
        				.addGroup(groupLayout.createSequentialGroup()
        					.addGap(75)
        					.addComponent(chooseBtn)))
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
        				.addGroup(groupLayout.createSequentialGroup()
        					.addComponent(btnCancel, GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        					.addGap(243))
        				.addGroup(groupLayout.createSequentialGroup()
        					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
        						.addComponent(evaluationTextField, Alignment.LEADING)
        						.addComponent(comboBox, Alignment.LEADING, 0, 139, Short.MAX_VALUE))
        					.addContainerGap())))
        );
        groupLayout.setVerticalGroup(
        	groupLayout.createParallelGroup(Alignment.TRAILING)
        		.addGroup(groupLayout.createSequentialGroup()
        			.addContainerGap(86, Short.MAX_VALUE)
        			.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(nameLabel)
        				.addComponent(evaluationTextField, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
        			.addGap(18)
        			.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(categoryLabel)
        				.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addGap(28)
        			.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(chooseBtn)
        				.addComponent(btnCancel))
        			.addGap(91))
        );
        panel.setLayout(groupLayout);
    }

    @Override
    public void setVisible(Boolean visible) {
        if(visible){
            MainFrame.getInstance().getFrame().getContentPane().add(panel);
            MainFrame.getInstance().getFrame().validate();
            MainFrame.getInstance().setPanel(panel);
            MainFrame.getInstance().getFrame().pack();
        }else{
            MainFrame.getInstance().getFrame().getContentPane().remove(panel);
        }
    }
}
