package UI.EmployeeUI;

import Authentication.Authentication;
import MetricManagement.MetricCatalog;
import MetricManagement.MetricCatalogProxy;
import MetricManagement.MetricFacade;
import UI.LoginForm;
import UI.MainFrame;
import UI.ProfilePanel;
import UI.Visibility;
import UserManagement.UserCatalog;
import UserManagement.UserCatalogProxy;
import UserManagement.UserFacade;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by parishad on 5/27/18.
 */
public class EmployeeMainFrame implements Visibility {
    private JMenuItem evalationResultMenuItem;
    private JMenuItem evaluationMenuItem;
    private JMenuItem logoutMenuItem;
    private UserFacade userFacade;
    private MetricFacade metricFacade;
    private JPanel panel;
    private boolean isEvaluator;

    public EmployeeMainFrame(boolean isEvaluator) {
        userFacade = new UserCatalogProxy();
        metricFacade = new MetricCatalogProxy();
        panel = new ProfilePanel().getPanel();
        this.isEvaluator = isEvaluator;

        createComponents();
        createEvents();
    }

    private void createEvents() {
        JFrame frame = MainFrame.getInstance().getFrame();

        logoutMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Authentication.getInstance().logout();
                LoginForm loginForm = new LoginForm();
                MainFrame.getInstance().getFrame().getContentPane().remove(MainFrame.getInstance().getPanel());
                loginForm.setVisible(true);
            }
        });
        evalationResultMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ShowEvaluationResultPanel showEvaluationResultPanel = new ShowEvaluationResultPanel();
                frame.setTitle("مشاهده‌ی ارزیابی‌های انجام شده");
                MainFrame.getInstance().getFrame().getContentPane().remove(MainFrame.getInstance().getPanel());
                showEvaluationResultPanel.setVisible(true);
            }
        });

        evaluationMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.setTitle("انتخاب کارمند برای ارزیابی");
                ShowEmployees showEmployees = new ShowEmployees(userFacade, metricFacade);
                MainFrame.getInstance().getFrame().getContentPane().remove(MainFrame.getInstance().getPanel());
                showEmployees.setVisible(true);
            }
        });
    }

    private void createComponents() {
        JFrame frame = MainFrame.getInstance().getFrame();
        JMenuBar menuBar = frame.getJMenuBar();
        menuBar.removeAll();


        logoutMenuItem = new JMenuItem("خروج از سامانه");
        menuBar.add(logoutMenuItem);

        evalationResultMenuItem = new JMenuItem("مشاهده ارزیابی‌های انجام شده");
        menuBar.add(evalationResultMenuItem);


        evaluationMenuItem = new JMenuItem("ارزیابی کارمندان");
        evaluationMenuItem.setEnabled(isEvaluator);
        menuBar.add(evaluationMenuItem);
        menuBar.setVisible(true);

    }

    @Override
    public void setVisible(Boolean visible) {
        if (visible) {
            MainFrame.getInstance().getFrame().getContentPane().add(panel);
            MainFrame.getInstance().getFrame().validate();
            MainFrame.getInstance().setPanel(panel);
            MainFrame.getInstance().getFrame().pack();
        } else {
            MainFrame.getInstance().getFrame().getContentPane().remove(panel);
        }
    }
}
