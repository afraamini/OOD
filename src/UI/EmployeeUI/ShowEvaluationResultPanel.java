package UI.EmployeeUI;

import Authentication.Authentication;
import EvaluationManagement.EvaluationCatalog;
import UI.CancelButton;
import UI.MainFrame;
import UI.Visibility;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by parishad on 5/27/18.
 */
public class ShowEvaluationResultPanel implements Visibility {
    private JTable table;
    private JPanel panel;
    private JButton cancelBtn;
    private String[] columnNames;
    Object[][] data;

    public ShowEvaluationResultPanel() {
        super();
        prepareData();
        createComponents();
        createEvents();
    }

    private void prepareData() {
        columnNames = new String[]{"ماهیت معیار",
                "دسته ارزیابی", "نتیجه ارزیابی"};
        data = EvaluationCatalog.getInstance().showEvaluationForEmployee(Authentication.getInstance().getCurrentUser());
    }

    public void createEvents(){
        new CancelButton(cancelBtn,this);
    }
    private void createComponents() {
        panel = new JPanel();
        GroupLayout groupLayout = new GroupLayout(panel);
        panel.setLayout(groupLayout);
        panel.setLayout(groupLayout);
        JScrollPane scrollPane = new JScrollPane();

        cancelBtn = new JButton("لغو عملیات");
        cancelBtn.setForeground(new Color(255, 20, 147));

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(GroupLayout.Alignment.TRAILING, groupLayout.createSequentialGroup()
                                .addGap(43)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 331, Short.MAX_VALUE)
                                .addComponent(cancelBtn, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                                .addGap(108))
                        .addGroup(groupLayout.createSequentialGroup()
                                .addGap(15)
                                .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 673, Short.MAX_VALUE)
                                .addGap(14))
        );
        groupLayout.setVerticalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addGap(18)
                                .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
                                .addGap(18)
                                .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(cancelBtn)
                                        )
                                .addContainerGap(37, Short.MAX_VALUE))
        );
        table = new JTable(data, columnNames);
        scrollPane.setViewportView(table);
    }

    public JPanel getPanel(){return panel;}

    @Override
    public void setVisible(Boolean visible) {
        if(visible){
            MainFrame.getInstance().getFrame().getContentPane().add(panel);
            MainFrame.getInstance().getFrame().validate();
            MainFrame.getInstance().setPanel(panel);
            MainFrame.getInstance().getFrame().pack();
        }else{
            MainFrame.getInstance().getFrame().getContentPane().remove(panel);
        }
    }
}
