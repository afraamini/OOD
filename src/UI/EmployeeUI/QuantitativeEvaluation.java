package UI.EmployeeUI;

import Authentication.Authentication;
import EvaluationManagement.EvaluationCatalog;
import EvaluationManagement.EvaluationFacade;
import MetricDataManagement.Metric;
import MetricManagement.MetricFacade;
import UI.*;
import UserDataManagement.SystemUser;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class QuantitativeEvaluation implements Visibility {
	private JPanel panel = new JPanel();
	private JButton addBtn;
	private JButton cancelBtn;
	private MetricFacade metricFacade;
	private String targetType;
	private SystemUser employeeToEvaluate;
	private String metricName;
	private String measure;
	private JTextArea result;
	private EvaluationFacade evaluationFacade;

	public QuantitativeEvaluation(MetricFacade metricFacade, String targetType, SystemUser employeeToEvaluate, String metricName) {
		this.metricFacade = metricFacade;
		this.targetType = targetType;
		this.employeeToEvaluate = employeeToEvaluate;
		this.metricName = metricName;

		evaluationFacade = EvaluationCatalog.getInstance();

		prepareData();
		createComponent();
		createEvents();
	}

	private void prepareData() {
		this.measure = metricFacade.findMeasureForMetric(metricName);

	}

	private void createEvents() {
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(result.getText().equals(""))
					new MainDialog(MessageType.NOT_COMPLETE, "لطفا ارزیابی را انجام دهید");
				else {
					Metric metric = metricFacade.findMetric(metricName);
					if(evaluationFacade.addEvaluation(employeeToEvaluate, metric,
							Authentication.getInstance().getCurrentUser(), result.getText()))
					    new MainDialog(MessageType.SUCCESS, "ارزیابی");
					else
					    new MainDialog(MessageType.ERROR,"ثبت ارزیابی");

				}

			}
		});

		new CancelButton(cancelBtn,this);
	}

	private void createComponent() {
		panel = new JPanel();

		addBtn = new JButton("ثبت ارزیابی");
		addBtn.setForeground(new Color(0, 102, 0));
		
		cancelBtn = new JButton("لغو عملیات");
		cancelBtn.setForeground(UIManager.getColor("Button.select"));
		
		JLabel nameLabel = new JLabel("نام معیار");
		
		JLabel experienceLabel = new JLabel(metricName);
		
		JLabel quantWayLabel = new JLabel("روش کمی ارزیابی");

		JLabel quantWayExplained = new JLabel(measure);


		
		JLabel quantLabel = new JLabel("ارزیابی کمی");
		
		result = new JTextArea();
		GroupLayout groupLayout = new GroupLayout(panel);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(43)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(quantWayLabel)
							.addGap(18)
							.addComponent(quantWayExplained, GroupLayout.PREFERRED_SIZE, 242, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(nameLabel)
							.addGap(64)
							.addComponent(experienceLabel, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
									.addComponent(addBtn)
									.addPreferredGap(ComponentPlacement.RELATED, 137, Short.MAX_VALUE)
									.addComponent(cancelBtn, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(quantLabel, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(result, GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)))))
					.addGap(2147483259)
					.addGap(118))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(15)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(nameLabel)
						.addComponent(experienceLabel, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(26)
							.addComponent(quantWayLabel)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(47)
									.addComponent(result, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(65)
									.addComponent(quantLabel))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(9)
							.addComponent(quantWayExplained, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)))
					.addGap(44)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(cancelBtn)
						.addComponent(addBtn))
					.addContainerGap(27, Short.MAX_VALUE))
		);

		panel.setLayout(groupLayout);
		
	}
	@Override
	public void setVisible(Boolean visible) {
		if(visible){
			MainFrame.getInstance().getFrame().getContentPane().add(panel);
			MainFrame.getInstance().getFrame().validate();
			MainFrame.getInstance().setPanel(panel);
		}else{
			MainFrame.getInstance().getFrame().getContentPane().remove(panel);
		}
	}

}
