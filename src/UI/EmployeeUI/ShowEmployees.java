package UI.EmployeeUI;

import Authentication.Authentication;
import MetricManagement.MetricFacade;
import UI.CancelButton;
import UI.MainFrame;
import UI.Visibility;
import UserDataManagement.SystemUser;
import UserManagement.UserFacade;
//import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class ShowEmployees implements Visibility {
    private JPanel panel;
    private JButton chooseBtn;
    private JButton cancelBtn;
    private ArrayList<SystemUser> employees;
    private Object[][] data;
    private String[] columnNames;
    private JTable table;
    private String targetEmployee;
    private UserFacade userFacade;
    private MetricFacade metricFacade;
    /**
     * Create the application.
     */
    public ShowEmployees(UserFacade userFacade, MetricFacade metricFacade) {
        this.userFacade = userFacade;
        this.metricFacade = metricFacade;
        this.data = userFacade.showAllEmployeesData(Authentication.getInstance().getCurrentUser().getPersonelID());
        prepareData();
        createComponents();
        createEvents();
    }

    private void prepareData() {
        columnNames = new String[]{"شماره پرسنلی","نام کارمند",
                " نام خانوادگی کارمند"};
    }
    public void createEvents() {
        chooseBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (targetEmployee != null) {
                    MainFrame.getInstance().getFrame().setTitle("مشاهده‌ی ارزیابی‌های انجام شده");
                    SystemUser employee = userFacade.getEmployee(targetEmployee);
                    ShowCategories showCategories = new ShowCategories(employee, metricFacade,userFacade);
                    setVisible(false);
                    showCategories.setVisible(true);
                }
            }
        });

        new CancelButton(cancelBtn,this);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = table.rowAtPoint(e.getPoint());
                targetEmployee = (String)data[row][0];
            }
        });

    }

    private void createComponents() {
        panel = new JPanel();
        GroupLayout groupLayout = new GroupLayout(panel);

        chooseBtn = new JButton("انتخاب کارمند برای ارزیابی");
        chooseBtn.setForeground(new Color(0, 128, 0));

        cancelBtn = new JButton("لغو عملیات");
        cancelBtn.setForeground(new Color(255, 20, 147));



        JScrollPane scrollPane = new JScrollPane();
        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addGap(30)
                                .addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
                                        .addGroup(groupLayout.createSequentialGroup()
                                                .addComponent(chooseBtn)
                                                .addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(cancelBtn, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                                        .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 482, GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(45, Short.MAX_VALUE))
        );
        groupLayout.setVerticalGroup(
                groupLayout.createParallelGroup(Alignment.TRAILING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(ComponentPlacement.RELATED)
                                .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                        .addComponent(chooseBtn)
                                        .addComponent(cancelBtn))
                                .addGap(261))
        );

        table = new JTable(data, columnNames);
        scrollPane.setViewportView(table);
        panel.setLayout(groupLayout);
    }

    public JPanel getPanel(){return panel;}

    @Override
    public void setVisible(Boolean visible) {
        if(visible){
            MainFrame.getInstance().getFrame().getContentPane().add(panel);
            MainFrame.getInstance().getFrame().validate();
            MainFrame.getInstance().setPanel(panel);
            MainFrame.getInstance().getFrame().pack();
        }else{
            MainFrame.getInstance().getFrame().getContentPane().remove(panel);
        }
    }
}
