package UI;

import Authentication.Authentication;
import UI.EmployeeUI.EmployeeMainFrame;
import UI.ManagerUI.ManagerMainPanel;
import Authentication.AccessLevel;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginForm implements Visibility {

    private JFrame frame;
    private JPanel panel;
    private JTextField IDTextField;
    private JPasswordField PassTextField;
    private JButton loginBtn;
    private Authentication authentication;

    /**
     * Create the application.
     */
    public LoginForm() {

        frame = MainFrame.getInstance().getFrame();
        frame.setTitle("ورود به حساب کاربری");
        panel = new JPanel();
        this.authentication = Authentication.getInstance();
        createComponents();
        createEvents();
    }

    public JFrame getFrame() {
        return frame;
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    LoginForm window = new LoginForm();
                    window.setVisible(true);
                    window.frame.setVisible(true);
                    window.frame.pack();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void navigateToUserPanel(String role) {
        switch (role) {
            case "None":
                System.out.println("Error...try again");
                break;
            case "Manager":
                ManagerMainPanel managerMainPanel = new ManagerMainPanel();
                frame.getContentPane().remove(MainFrame.getInstance().getPanel());
                managerMainPanel.setVisible(true);
                frame.setTitle("پنل کاربری مدیر");
                break;
            case "Evaluator":
            case "Employee":
                EmployeeMainFrame employeeMainFrame = new EmployeeMainFrame(role.equals("Evaluator"));
                frame.getContentPane().remove(MainFrame.getInstance().getPanel());
                employeeMainFrame.setVisible(true);
                frame.setTitle("پنل کاربری کارمند");
                break;
        }
    }

    private void login() {
        String personnelID = IDTextField.getText();
        String password = "";
        for (Character c : PassTextField.getPassword())
            password += c.toString();
        String role = authentication.login(personnelID, password);
        navigateToUserPanel(role);
    }

    private void createEvents() {
        loginBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                login();
            }
        });
    }

    private void createComponents() {
        JFrame frame = getFrame();
        frame.getJMenuBar().setVisible(false);

        JLabel IDLabel = new JLabel("شماره پرسنلی");
        IDLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 16));

        JLabel passLabel = new JLabel("رمز عبور");
        passLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 16));

        IDTextField = new JTextField();
        IDTextField.setColumns(10);

        PassTextField = new JPasswordField();
        PassTextField.setEchoChar('*');
        PassTextField.setColumns(10);

        loginBtn = new JButton("وارد شو");
        loginBtn.setForeground(new Color(0, 102, 0));
        GroupLayout groupLayout = new GroupLayout(panel);
        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                        .addGroup(groupLayout.createSequentialGroup()
                                                .addGap(29)
                                                .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                                        .addGroup(groupLayout.createSequentialGroup()
                                                                .addComponent(passLabel, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
                                                                .addGap(18)
                                                                .addComponent(PassTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(groupLayout.createSequentialGroup()
                                                                .addComponent(IDLabel, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
                                                                .addGap(18)
                                                                .addComponent(IDTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(groupLayout.createSequentialGroup()
                                                .addGap(122)
                                                .addComponent(loginBtn)))
                                .addContainerGap(54, Short.MAX_VALUE))
        );
        groupLayout.setVerticalGroup(
                groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addGap(23)
                                .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                        .addComponent(IDLabel, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(IDTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addGap(34)
                                .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                        .addComponent(passLabel, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(PassTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                                .addComponent(loginBtn)
                                .addGap(36))
        );
        panel.setLayout(groupLayout);
    }

    @Override
    public void setVisible(Boolean visible) {
        if (visible) {
            MainFrame.getInstance().getFrame().getContentPane().add(panel);
            MainFrame.getInstance().getFrame().validate();
            MainFrame.getInstance().setPanel(panel);
            MainFrame.getInstance().getFrame().pack();
        } else {
            MainFrame.getInstance().getFrame().getContentPane().remove(panel);
        }
    }
}
