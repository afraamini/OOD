package UI;
import javax.swing.*;

public class MainDialog{
    private JDialog mainDialog;

    public MainDialog(MessageType type,String message) {
        mainDialog = new JDialog();
        mainDialog.setLocationRelativeTo(MainFrame.getInstance().getFrame());
        informUser(type, message);
    }

    private void informUser(MessageType type,String message){
        if(type == MessageType.SUCCESS)
        {
            JOptionPane.showMessageDialog(mainDialog, message+" با موفقیت انجام شد.", "اعلان", JOptionPane.INFORMATION_MESSAGE);

        }else if(type == MessageType.ERROR){
            JOptionPane.showMessageDialog(mainDialog, message+" با خطا مواجه شد.", "خطا", JOptionPane.ERROR_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(mainDialog, message, "اخطار", JOptionPane.WARNING_MESSAGE);
        }
    }
}

