package UI.ManagerUI;

import MetricDataManagement.Metric;
import MetricManagement.MetricFacade;
import UI.*;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CategorizeAEvaluation implements Visibility {

	private JPanel panel;
	private JButton addBtn;
	private JButton cancelBtn;

	private Metric metric;
	private MetricFacade metricFacade;

	private JRadioButton radioButtonQuan;
	private JRadioButton radioButtonQual;
	private JRadioButton radioButtonMulti;

	private JTextPane textPaneQuanMeasure, textPaneQuanInterpretation, textPaneQualMeasure, textPaneQualInterpretation;
	private JLabel labelQuanMeasure, labelQualMeasure, labelQuanInterpretation, labelQualInterpretation;
	/**
	 * Create the application.
	 */
	public CategorizeAEvaluation(Metric metric, MetricFacade metricFacade) {
		this.metric = metric;
		this.metricFacade = metricFacade;
		createComponents();
		createEvents();
	}


	private void enableFields(String type) {
		if (type.equals("Quan")) {
			textPaneQuanInterpretation.setEditable(true);
			textPaneQuanMeasure.setEditable(true);
			textPaneQualInterpretation.setEditable(false);
			textPaneQualMeasure.setEditable(false);

			labelQuanInterpretation.setEnabled(true);
			labelQuanMeasure.setEnabled(true);
			labelQualInterpretation.setEnabled(false);
			labelQualMeasure.setEnabled(false);
		} else if (type.equals("Qual")) {
			textPaneQuanInterpretation.setEditable(false);
			textPaneQuanMeasure.setEditable(false);
			textPaneQualInterpretation.setEditable(true);
			textPaneQualMeasure.setEditable(true);

			labelQualInterpretation.setEnabled(true);
			labelQualMeasure.setEnabled(true);
			labelQuanInterpretation.setEnabled(false);
			labelQuanMeasure.setEnabled(false);
		} else if (type.equals("Multi")) {
			textPaneQualInterpretation.setEditable(true);
			textPaneQualMeasure.setEditable(true);
			textPaneQuanInterpretation.setEditable(true);
			textPaneQuanMeasure.setEditable(true);

			labelQualInterpretation.setEnabled(true);
			labelQualMeasure.setEnabled(true);
			labelQuanInterpretation.setEnabled(true);
			labelQuanMeasure.setEnabled(true);
		}
	}

	private void createEvents() {
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onSubmit();
			}
		});
		new CancelButton(cancelBtn,this);
		radioButtonQuan.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (radioButtonQuan.isSelected()) {
					enableFields("Quan");
				}
			}
		});
		radioButtonQual.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (radioButtonQual.isSelected()) {
					enableFields("Qual");
				}
			}
		});
		radioButtonMulti.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (radioButtonMulti.isSelected()) {
					enableFields("Multi");
				}
			}
		});
	}
	private void onSubmit(){

		String measure = "", interpretation = "", type = "";
		if (radioButtonQuan.isSelected()) {
			measure = textPaneQuanMeasure.getText();
			interpretation = textPaneQuanInterpretation.getText();
			type = "Quan";
			if (measure.equals(""))
				textPaneQuanMeasure.setText("لطفا روش ارزیابی را وارد کنید.");
			if (interpretation.equals(""))
				textPaneQuanInterpretation.setText("لطفا تفسیر ارزیابی را وارد کنید.");
		} else if (radioButtonQual.isSelected()) {
			measure = textPaneQualMeasure.getText();
			interpretation = textPaneQualInterpretation.getText();
			type = "Qual";
			if (measure.equals(""))
				textPaneQualMeasure.setText("لطفا روش ارزیابی را وارد کنید.");
			if (interpretation.equals(""))
				textPaneQualInterpretation.setText("لطفا تفسیر ارزیابی را وارد کنید.");
		} else if (radioButtonMulti.isSelected()){
			String qualMeasure = textPaneQualMeasure.getText();
			String qualInterpretation = textPaneQualInterpretation.getText();
			type = "Multi";
			if (qualMeasure.equals(""))
				textPaneQuanMeasure.setText("لطفا روش ارزیابی کمی را وارد کنید.");
			if (qualInterpretation.equals(""))
				textPaneQuanInterpretation.setText("لطفا تفسیر ارزیابی کمی را وارد کنید.");

			String quanMeasure = textPaneQuanMeasure.getText();
			String quanInterpretation = textPaneQuanInterpretation.getText();

			if (quanMeasure.equals(""))
				textPaneQualMeasure.setText("لطفا روش ارزیابی کیفی را وارد کنید.");
			if (quanInterpretation.equals(""))
				textPaneQualInterpretation.setText("لطفا تفسیر ارزیابی کیفی را وارد کنید.");
			if(!qualInterpretation.equals("")&& !quanInterpretation.equals(""))
				interpretation = "تفسیر کیفی: \n"+qualInterpretation+"\n تفسیر کمی\n"+quanInterpretation;
			if(!qualMeasure.equals("") && !quanMeasure.equals(""))
				measure = "روش کیفی\n"+qualMeasure+"\n روش کمی \n"+quanMeasure;
		}
		if (!measure.equals("") && !interpretation.equals("")){
			if(metricFacade.setMetricType(metric, type, measure, interpretation))
				new MainDialog(MessageType.SUCCESS, "دسته بندی معیار");
			else new MainDialog(MessageType.ERROR,"دسته‌بندی معیار");
		}
	}
	private void createComponents() {
		panel = new JPanel();

		addBtn = new JButton("ثبت معیار");
		addBtn.setForeground(new Color(0, 128, 0));

		cancelBtn = new JButton("لغو عملیات");
		cancelBtn.setForeground(new Color(255, 20, 147));

		ButtonGroup buttonGroup = new ButtonGroup();
		radioButtonQuan = new JRadioButton("کمی");
		radioButtonQuan.setSelected(true);
		buttonGroup.add(radioButtonQuan);

		JLabel lblNewLabel = new JLabel("معیار ارزیابی");

		radioButtonQual = new JRadioButton("کیفی");
		buttonGroup.add(radioButtonQual);

		radioButtonMulti = new JRadioButton("کمی-کیفی");
		buttonGroup.add(radioButtonMulti);

		labelQuanMeasure = new JLabel("روش کمی ارزیابی");

		labelQualMeasure = new JLabel("روش کیفی ارزیابی");
		labelQualMeasure.setEnabled(false);

		labelQuanInterpretation = new JLabel("نحوه تفسیر کمی");

		labelQualInterpretation = new JLabel("نحوه تفسیر کیفی");
		labelQualInterpretation.setEnabled(false);

		textPaneQuanMeasure = new JTextPane();
//		textPaneQuanMeasure.setText("مژر کمی");

		textPaneQuanInterpretation = new JTextPane();
//		textPaneQuanInterpretation.setText("تفس کمی");

		textPaneQualMeasure = new JTextPane();
		textPaneQualMeasure.setEditable(false);
//		textPaneQualMeasure.setText("مژ کیفی");

		textPaneQualInterpretation = new JTextPane();
//		textPaneQualInterpretation.setText("تفسیر کیفی");
		textPaneQualInterpretation.setEditable(false);

		GroupLayout groupLayout = new GroupLayout(panel);
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(82)
												.addComponent(addBtn))
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(27)
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addGroup(groupLayout.createSequentialGroup()
																.addComponent(lblNewLabel)
																.addGap(49)
																.addComponent(radioButtonQuan))
														.addGroup(groupLayout.createSequentialGroup()
																.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
																		.addComponent(labelQualMeasure, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
																		.addComponent(labelQuanMeasure, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))
																.addGap(18)
																.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
																		.addComponent(textPaneQualMeasure, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
																		.addComponent(textPaneQuanMeasure, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE))))))
								.addGap(23)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
												.addComponent(radioButtonQual, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addGroup(groupLayout.createSequentialGroup()
																.addGap(96)
																.addComponent(cancelBtn, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
														.addGroup(groupLayout.createSequentialGroup()
																.addPreferredGap(ComponentPlacement.RELATED)
																.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
																		.addComponent(textPaneQuanInterpretation, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
																		.addComponent(radioButtonMulti, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)))))
										.addComponent(labelQuanInterpretation, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
										.addGroup(groupLayout.createSequentialGroup()
												.addComponent(labelQualInterpretation, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
												.addGap(18)
												.addComponent(textPaneQualInterpretation, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)))
								.addContainerGap(25, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
								.addGap(39)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblNewLabel)
										.addComponent(radioButtonMulti, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
										.addComponent(radioButtonQual, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
										.addComponent(radioButtonQuan))
								.addGap(32)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(labelQuanMeasure)
														.addComponent(labelQuanInterpretation))
												.addComponent(textPaneQuanMeasure, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
										.addComponent(textPaneQuanInterpretation, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(22)
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addGroup(groupLayout.createSequentialGroup()
																.addGap(23)
																.addComponent(labelQualMeasure))
														.addGroup(groupLayout.createSequentialGroup()
																.addPreferredGap(ComponentPlacement.RELATED)
																.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
																		.addComponent(textPaneQualInterpretation, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
																		.addComponent(textPaneQualMeasure, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))))
												.addPreferredGap(ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(addBtn)
														.addComponent(cancelBtn))
												.addGap(32))
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(43)
												.addComponent(labelQualInterpretation)
												.addContainerGap())))
		);
		panel.setLayout(groupLayout);
	}

	@Override
	public void setVisible(Boolean visible) {
		if(visible){
			MainFrame.getInstance().getFrame().getContentPane().add(panel);
			MainFrame.getInstance().getFrame().validate();
			MainFrame.getInstance().setPanel(panel);
			MainFrame.getInstance().getFrame().pack();
		}else{
			MainFrame.getInstance().getFrame().getContentPane().remove(panel);
		}
	}
}
