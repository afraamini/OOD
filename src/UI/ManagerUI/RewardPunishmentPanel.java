package UI.ManagerUI;

import MetricDataManagement.Metric;
import MetricManagement.MetricFacade;
import UI.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by parishad on 5/27/18.
 */
public class RewardPunishmentPanel implements Visibility {
    private JPanel panel;
    private JButton addBtn;
    private JButton cancelBtn;
    private Metric metric;
    private MetricFacade metricFacade;
    private JTextPane rewardTextPane;
    private JTextPane punishmentTextPane;

    public RewardPunishmentPanel(Metric metric, MetricFacade metricFacade){
        this.metric = metric;
        this.metricFacade = metricFacade;
        createComponents();
        createEvents();
    }

    private void createEvents() {
        addBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String reward = rewardTextPane.getText();
                String punishment = punishmentTextPane.getText();
                if(reward.isEmpty() || punishment.isEmpty())
                    new MainDialog(MessageType.NOT_COMPLETE,"روش تشویق و تنبیه را وارد کنید.");
                else{
                    if(metricFacade.setRewardPunishment(metric,reward,punishment))
                        new MainDialog(MessageType.SUCCESS,"ثبت روش تشویق و تنبیه");
                    else new MainDialog(MessageType.ERROR,"ثبت روش تشویق و تنبیه");
                }
            }
        });
        new CancelButton(cancelBtn,this);
    }

    private void createComponents() {
        panel = new JPanel();
        GroupLayout groupLayout = new GroupLayout(panel);
        panel.setLayout(groupLayout);

        addBtn = new JButton("ثبت روش‌ها");
        addBtn.setForeground(new Color(0, 128, 0));

        cancelBtn = new JButton("لغو عملیات");
        cancelBtn.setForeground(new Color(255, 20, 147));

        JLabel rewardLbl = new JLabel("روش تشویق");
        JLabel punishLbl = new JLabel("روش تنبیه");
        rewardTextPane = new JTextPane();

        punishmentTextPane = new JTextPane();
        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addGap(27)
                                .addComponent(rewardLbl, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rewardTextPane, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                                .addGap(26)
                                .addComponent(punishLbl, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(punishmentTextPane, GroupLayout.PREFERRED_SIZE, 149, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(GroupLayout.Alignment.TRAILING, groupLayout.createSequentialGroup()
                                .addContainerGap(149, Short.MAX_VALUE)
                                .addComponent(addBtn)
                                .addGap(122)
                                .addComponent(cancelBtn, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                                .addGap(137))
        );
        groupLayout.setVerticalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addGap(30)
                                .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(punishmentTextPane, GroupLayout.Alignment.LEADING)
                                        .addGroup(GroupLayout.Alignment.LEADING, groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                .addComponent(rewardTextPane, GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
                                                .addComponent(punishLbl))
                                        .addComponent(rewardLbl, GroupLayout.Alignment.LEADING))
                                .addGap(33)
                                .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(cancelBtn)
                                        .addComponent(addBtn))
                                .addContainerGap(150, Short.MAX_VALUE))
        );
    }

    @Override
    public void setVisible(Boolean visible) {
        if(visible){
            MainFrame.getInstance().getFrame().getContentPane().add(panel);
            MainFrame.getInstance().getFrame().validate();
            MainFrame.getInstance().setPanel(panel);
            MainFrame.getInstance().getFrame().pack();
        }else{
            MainFrame.getInstance().getFrame().getContentPane().remove(panel);
        }
    }
}
