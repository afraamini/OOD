package UI.ManagerUI;

import MetricDataManagement.Type;
import MetricManagement.MetricFacade;
import UI.*;
import UserManagement.UserFacade;
import Utility.Translation;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ShowCategories implements Visibility {

	private JPanel panel;
	private JButton btnAdd;
	private JButton btnCancel;
	private UserFacade userFacade;
	private MetricFacade metricFacade;
	private Object[][] data;
	private String[] columnNames;
	private JTable table;
	private String targetType = null;

	/**
	 * Create the application.
	 */
	public ShowCategories(MetricFacade metricFacade, UserFacade userFacade) {
		this.data = metricFacade.showMetricTypes();
		this.userFacade = userFacade;
		this.metricFacade = metricFacade;

		createComponents();
		createEvents();
	}
	private void onSubmit(){
		if (targetType != null) {
			setVisible(false);
			Type type = metricFacade.findType(targetType);
			ShowACategory showACategory = new ShowACategory(type, userFacade, metricFacade);
			showACategory.setVisible(true);
		}else new MainDialog(MessageType.NOT_COMPLETE,"یک گزینه انتخاب کنید.");

	}
	private void createEvents() {
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onSubmit();
			}
		});
		new CancelButton(btnCancel,this);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = table.rowAtPoint(e.getPoint());
				targetType = Translation.translateMetricType((String)data[row][0]);
			}
		});
	}

	private void createComponents() {
		panel = new JPanel();
		
		btnAdd = new JButton("انتخاب دسته معیار");
		btnAdd.setForeground(new Color(0, 128, 0));
		
		btnCancel = new JButton("لغو عملیات");
		btnCancel.setForeground(new Color(255, 20, 147));
		
		columnNames = new String[]{"دسته ارزیابی",
                "تعداد معیار‌های اختصاص داده شده", "تعداد ارزیاب‌های اختصاص داده شده"};

		JScrollPane scrollPane = new JScrollPane();
		GroupLayout groupLayout = new GroupLayout(panel);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(30)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 482, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(96)
							.addComponent(btnAdd)
							.addGap(103)
							.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(32, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnAdd)
						.addComponent(btnCancel))
					.addGap(261))
		);
		
		table = new JTable(data, columnNames);
		scrollPane.setViewportView(table);
		panel.setLayout(groupLayout);

	}
	@Override
	public void setVisible(Boolean visible) {
		if(visible){
			MainFrame.getInstance().getFrame().getContentPane().add(panel);
			MainFrame.getInstance().getFrame().validate();
			MainFrame.getInstance().setPanel(panel);
			MainFrame.getInstance().getFrame().pack();
		}else{
			MainFrame.getInstance().getFrame().getContentPane().remove(panel);
		}
	}
}
