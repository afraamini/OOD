package UI.ManagerUI;

import MetricDataManagement.Type;
import MetricManagement.MetricFacade;
import UI.*;
import UserDataManagement.SystemUser;
import UserManagement.UserFacade;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by parishad on 5/27/18.
 */
public class AddNewEvaluatorPanel implements Visibility {
    private JButton addBtn;
    private JButton cancelBtn;
    private JPanel panel;
    private String[] columnNames;
    private Object[][] data;
    private String target = null;
    private JTable table;
    private UserFacade userFacade;
    private MetricFacade metricFacade;
    private Type type;

    public AddNewEvaluatorPanel(UserFacade userFacade, MetricFacade metricFacade, Object[][] data, Type type) {
        this.data = data;
        this.userFacade = userFacade;
        this.metricFacade = metricFacade;
        this.type = type;
        prepareData();
        createComponents();
        createEvents();
    }

    private void prepareData() {
        columnNames = new String[]{"نام کارمند", "نام خانوادگی کارمند", "شماره پرسنلی"};
    }
    private void onSubmit(){
        if(target == null) new MainDialog(MessageType.NOT_COMPLETE,"یک گزینه انتخاب کنید.");
        else{
            SystemUser targetEmp = userFacade.getEmployee(target);
            if(metricFacade.addEmployeeToType(targetEmp, type)){
                new MainDialog(MessageType.SUCCESS,"اضافه کردن ارزیاب جدید");
                userFacade.updateEvaluators();
            }else new MainDialog(MessageType.ERROR,"اضافه کردن ارزیاب جدید");
        }
    }
    private void createEvents() {
        addBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onSubmit();
            }
        });
        new CancelButton(cancelBtn,this);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = table.rowAtPoint(e.getPoint());
                target = (String)data[row][2];
            }
        });
    }

    private void createComponents() {
        panel = new JPanel();
        GroupLayout groupLayout = new GroupLayout(panel);
        panel.setLayout(groupLayout);

        addBtn = new JButton("افزودن کارمند به لیست ارزیابان دسته");
        addBtn.setForeground(new Color(0, 128, 0));

        cancelBtn = new JButton("لغو عملیات");
        cancelBtn.setForeground(new Color(255, 20, 147));


        JScrollPane scrollPane = new JScrollPane();
        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addGap(30)
                                .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addGroup(groupLayout.createSequentialGroup()
                                                .addComponent(addBtn)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(cancelBtn, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                                        .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 482, GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(45, Short.MAX_VALUE))
        );
        groupLayout.setVerticalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(addBtn)
                                        .addComponent(cancelBtn))
                                .addGap(261))
        );

        table = new JTable(data, columnNames);
        scrollPane.setViewportView(table);
    }

    @Override
    public void setVisible(Boolean visible) {
        if (visible) {
            MainFrame.getInstance().getFrame().getContentPane().add(panel);
            MainFrame.getInstance().getFrame().validate();
            MainFrame.getInstance().setPanel(panel);
            MainFrame.getInstance().getFrame().pack();
        } else {
            MainFrame.getInstance().getFrame().getContentPane().remove(panel);
        }
    }
}
