package UI.ManagerUI;

import UI.MainDialog;
import UI.MessageType;
import UserManagement.UserFacade;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by parishad on 5/27/18.
 */
public class AddUnitManagerPanel extends AddUserPanel {

    public AddUnitManagerPanel(UserFacade userFacade) {
        super(userFacade);
        createEvents();
    }

    private void createEvents(){
        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getData();
                if(firstName.isEmpty() || lastName.isEmpty() || personelID.isEmpty() || password.isEmpty()){
                    new MainDialog(MessageType.NOT_COMPLETE,"فیلدهای ستاره‌دار را تکمیل کنید.");
                }
                else{
                    if(userFacade.addManager(firstName,lastName,email,personelID,
                            responsiblities,unit, password)){
                        new MainDialog(MessageType.SUCCESS,"ثبت کارمند");
                        userFacade.updateEmployees();
                    }
                    else
                        new MainDialog(MessageType.ERROR,"ثبت کارمند");
                }
            }
        });
    }
}
