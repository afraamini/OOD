package UI.ManagerUI;

import MetricManagement.MetricFacade;

import MetricDataManagement.Type;
import UI.CancelButton;
import UI.MainFrame;
import UI.Visibility;
import UserManagement.UserFacade;
import Utility.Translation;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ShowACategory implements Visibility {

	private JPanel panel;
	private JButton btnAdd;
	private JButton btnCancel;
	private UserFacade userFacade;
	private MetricFacade metricFacade;
	private Type type;

	/**
	 * Create the application.
	 */
	public ShowACategory(Type type, UserFacade userFacade, MetricFacade metricFacade) {
		this.type = type;
		this.userFacade = userFacade;
		this.metricFacade = metricFacade;
		createComponents();
		createEvents();
	}
	private void onSelect(){
		setVisible(false);

		Object[][] data = userFacade.showAllEvaluatorsExceptType(type.getIdNumber());
		AddEvaluatorPanel addEvaluatorPanel = new AddEvaluatorPanel(metricFacade, userFacade, data, type);
		addEvaluatorPanel.setVisible(true);
	}
	private void createEvents() {
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onSelect();}
		});

		new CancelButton(btnCancel,this);
	}

	private void createComponents() {
		panel = new JPanel();
		
		btnAdd = new JButton("افزودن ارزیاب به این دسته");
		btnAdd.setForeground(new Color(0, 128, 0));
		
		btnCancel = new JButton("لغو عملیات");
		btnCancel.setForeground(new Color(255, 20, 147));

		
		JLabel nameLabel = new JLabel("نام دسته");
		nameLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 14));
		
		JTextField typeTextField = new JTextField();
		typeTextField.setHorizontalAlignment(SwingConstants.CENTER);
		typeTextField.setEditable(false);
		typeTextField.setText(Translation.translateMetricType(type.getName()));
		typeTextField.setColumns(10);
		
		JLabel evaluationInCategoryText = new JLabel("معیار‌های تعریف شده در دسته");
		evaluationInCategoryText.setFont(new Font("Lucida Grande", Font.PLAIN, 14));
		
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		String metricTypesStr = metricFacade.showMetricTypes(type.getName());
		textArea.setText(metricTypesStr);
		
		JLabel evaluatorsInCategory = new JLabel("تعداد ارزیابان منتسب به دسته");
		evaluatorsInCategory.setFont(new Font("Lucida Grande", Font.PLAIN, 14));

		JLabel textArea_1 = new JLabel();
		textArea_1.setText(metricFacade.getEvaluatorLen(type));
		GroupLayout groupLayout = new GroupLayout(panel);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addGap(75)
							.addComponent(btnAdd)
							.addPreferredGap(ComponentPlacement.RELATED, 82, Short.MAX_VALUE)
							.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(34)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(evaluatorsInCategory, GroupLayout.PREFERRED_SIZE, 157, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(textArea_1, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(evaluationInCategoryText, GroupLayout.PREFERRED_SIZE, 157, GroupLayout.PREFERRED_SIZE)
										.addComponent(nameLabel))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(typeTextField, GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
										.addComponent(textArea, GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE))))))
					.addGap(143))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(22, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(nameLabel)
						.addComponent(typeTextField, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(evaluationInCategoryText)
						.addComponent(textArea, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(evaluatorsInCategory, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
						.addComponent(textArea_1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
					.addGap(28)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnAdd)
						.addComponent(btnCancel))
					.addGap(91))
		);

		panel.setLayout(groupLayout);
	}


	@Override
	public void setVisible(Boolean visible) {
		if(visible){
			MainFrame.getInstance().getFrame().getContentPane().add(panel);
			MainFrame.getInstance().getFrame().validate();
			MainFrame.getInstance().setPanel(panel);
			MainFrame.getInstance().getFrame().pack();
		}else{
			MainFrame.getInstance().getFrame().getContentPane().remove(panel);
		}
	}
}
