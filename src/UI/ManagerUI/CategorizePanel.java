package UI.ManagerUI;

import MetricDataManagement.Metric;
import MetricManagement.MetricFacade;
import UI.CancelButton;
import UI.MainFrame;
import UI.Visibility;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by parishad on 5/27/18.
 */
public class CategorizePanel implements Visibility {
    private JPanel panel;
    private JButton addBtn;
    private JButton cancelBtn;
    private String targetMetric;
    private MetricFacade metricFacade;
    private Object[][] data;
    private String[] columnNames;
    private JTable table;

    public CategorizePanel(Object[][] data, MetricFacade metricFacade) {
        this.data = data;
        this.metricFacade = metricFacade;
        targetMetric = null;
        prepareData();
        createComponents();
        createEvents();
    }
    private void prepareData() {
        columnNames = new String[]{"ماهیت معیار", "دسته ارزیابی"};
    }

    private void createEvents(){
        addBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onSubmit();
            }
        });
        new CancelButton(cancelBtn,this);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = table.rowAtPoint(e.getPoint());
                targetMetric = (String)data[row][0];
            }
        });

    }
    private void onSubmit(){
        if (targetMetric != null) {
            Metric metric = metricFacade.findMetric(targetMetric);
            if (metric.getMetricType()==null){
                MainFrame.getInstance().getFrame().setTitle("دسته بندی یک معیار");
                CategorizeAEvaluation categorizeAEvaluation = new CategorizeAEvaluation(metric, metricFacade);
                setVisible(false);
                categorizeAEvaluation.setVisible(true);
            }
        }
    }
    private void createComponents() {
        panel = new JPanel();
        GroupLayout groupLayout = new GroupLayout(panel);
        panel.setLayout(groupLayout);

        addBtn = new JButton("انتخاب معیار");
        addBtn.setForeground(new Color(0, 128, 0));

        cancelBtn = new JButton("لغو عملیات");
        cancelBtn.setForeground(new Color(255, 20, 147));


        JScrollPane scrollPane = new JScrollPane();
        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(groupLayout.createSequentialGroup()
                                                .addGap(30)
                                                .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 481, GroupLayout.PREFERRED_SIZE))
                                        .addGroup(groupLayout.createSequentialGroup()
                                                .addGap(100)
                                                .addComponent(addBtn)
                                                .addGap(107)
                                                .addComponent(cancelBtn, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(33, Short.MAX_VALUE))
        );
        groupLayout.setVerticalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addContainerGap(23, Short.MAX_VALUE)
                                .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)
                                .addGap(18)
                                .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(cancelBtn)
                                        .addComponent(addBtn))
                                .addGap(214))
        );

        table = new JTable(data, columnNames);
        scrollPane.setViewportView(table);
    }


    @Override
    public void setVisible(Boolean visible) {
        if(visible){
            MainFrame.getInstance().getFrame().getContentPane().add(panel);
            MainFrame.getInstance().getFrame().validate();
            MainFrame.getInstance().setPanel(panel);
            MainFrame.getInstance().getFrame().pack();
        }else{
            MainFrame.getInstance().getFrame().getContentPane().remove(panel);
        }
    }
}
