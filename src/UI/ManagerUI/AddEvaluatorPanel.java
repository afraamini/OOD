package UI.ManagerUI;

import MetricDataManagement.Type;
import MetricManagement.MetricFacade;
import UI.*;
import UserDataManagement.SystemUser;
import UserManagement.UserFacade;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by parishad on 5/27/18.
 */
public class AddEvaluatorPanel implements Visibility {
    private JPanel panel;
    private JButton addThisEvaluatorBtn;
    private JButton cancelBtn;
    private JTable table;
    private UserFacade userFacade;
    private String[] columnNames;
    private Object[][] data;
    private String target = null;
    private Type type;
    private MetricFacade metricFacade;

    protected JButton addNewEvaluatorBtn;

    public AddEvaluatorPanel(MetricFacade metricFacade, UserFacade userFacade, Object[][] data, Type type) {
        this.userFacade = userFacade;
        this.data = data;
        this.metricFacade = metricFacade;
        this.type = type;
        prepareData();
        createComponents();
        createEvents();
    }

    private void prepareData() {
        columnNames = new String[]{"نام ارزیاب", "نام خانوادگی ارزیاب", "شماره پرسنلی"};

    }
    private void onAddThisEvaluator(){
        if(target == null) new MainDialog(MessageType.NOT_COMPLETE,"یک گزینه انتخاب کنید.");
        else{
            SystemUser targetEval = userFacade.getEmployee(target);
            if(metricFacade.addEvaluatorToType(targetEval, type)) new MainDialog(MessageType.SUCCESS,"اضافه کردن ارزیاب");
            else new MainDialog(MessageType.ERROR,"اضافه کردن ارزیاب");
        }
    }
    private void onAddNewEvaluator(){
        Object[][] data2 = userFacade.showAllNonEvaluators();
        AddNewEvaluatorPanel addNewEvaluatorPanel = new AddNewEvaluatorPanel(userFacade, metricFacade, data2, type);
        MainFrame.getInstance().getFrame().setTitle("انتخاب ارزیاب از لیست کارمندان");
        setVisible(false);
        addNewEvaluatorPanel.setVisible(true);
    }
    private void createEvents() {
        addThisEvaluatorBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onAddThisEvaluator();
            }
        });

        addNewEvaluatorBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAddNewEvaluator();
            }
        });
        new CancelButton(cancelBtn,this);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = table.rowAtPoint(e.getPoint());
                target = (String)data[row][2];

            }
        });
    }

    private void createComponents() {
        panel = new JPanel();
        GroupLayout groupLayout = new GroupLayout(panel);
        panel.setLayout(groupLayout);

        addThisEvaluatorBtn = new JButton("افزودن این ارزیاب به دسته ارزیابی");
        addThisEvaluatorBtn.setForeground(new Color(0, 128, 0));

        cancelBtn = new JButton("لغو عملیات");
        cancelBtn.setForeground(new Color(255, 20, 147));

        JScrollPane scrollPane = new JScrollPane();

        addNewEvaluatorBtn = new JButton("انتخاب ارزیاب از لیست کارمندان");
        addNewEvaluatorBtn.setForeground(new Color(153, 51, 255));

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addGap(30)
                                .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(groupLayout.createSequentialGroup()
                                                .addComponent(addNewEvaluatorBtn, GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(addThisEvaluatorBtn)
                                                .addGap(8)
                                                .addComponent(cancelBtn, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                                        .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 482, GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(28, Short.MAX_VALUE))
        );
        groupLayout.setVerticalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(cancelBtn)
                                        .addComponent(addThisEvaluatorBtn)
                                        .addComponent(addNewEvaluatorBtn))
                                .addGap(261))
        );


        table = new JTable(data, columnNames);
        scrollPane.setViewportView(table);

    }
    @Override
    public void setVisible(Boolean visible) {
        if(visible){
            MainFrame.getInstance().getFrame().getContentPane().add(panel);
            MainFrame.getInstance().getFrame().validate();
            MainFrame.getInstance().setPanel(panel);
            MainFrame.getInstance().getFrame().pack();
        }else{
            MainFrame.getInstance().getFrame().getContentPane().remove(panel);
        }
    }
}
