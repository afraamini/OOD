package UI.ManagerUI;

import Authentication.Authentication;
import MetricManagement.MetricCatalog;
import MetricManagement.MetricCatalogProxy;
import MetricManagement.MetricFacade;
import UI.LoginForm;
import UI.MainFrame;
import UI.ProfilePanel;
import UI.Visibility;
import UserManagement.UserCatalog;
import UserManagement.UserCatalogProxy;
import UserManagement.UserFacade;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by parishad on 5/27/18.
 */
public class ManagerMainPanel implements Visibility {
    private JPanel panel;
    private JMenu evaluationMenu;
    private JMenuItem logoutMenuItem;
    private JMenuItem addEvaluationMenuItem;
    private JMenuItem categorizeMenuItem;
    private JMenuItem setPunishmentRewardMenuItem;
    private JMenu addEmployeeMenu;
    private JMenuItem addUnitManagerMenuItem;
    private JMenuItem addEmployeeMenuItem;
    private JMenuItem addEvaluatorToEvaluationMenuItem;
    private MetricFacade metricFacade;
    private UserFacade userFacade;
    private JFrame frame;

    public ManagerMainPanel(){
        metricFacade = new MetricCatalogProxy();
        userFacade = new UserCatalogProxy();
        panel = new ProfilePanel().getPanel();
        frame = MainFrame.getInstance().getFrame();
        createComponents();
        createEvents();
    }


    private void createEvents() {
        addUnitManagerMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { navigateToAddManager();}
        });
        addEmployeeMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                navigateToAddEmployee();
            }
        });
        addEvaluatorToEvaluationMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                navigateToAddEvaluator();
            }
        });
        addEvaluationMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                navigateToAddEvaluation();
            }
        });
        categorizeMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                navigateToCategorize();
            }
        });
        setPunishmentRewardMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                navigateToRewardPunishment();
            }
        });
        logoutMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logout();
            }
        });

    }

    private void logout() {
        setVisible(false);
        Authentication.getInstance().logout();
        LoginForm loginForm = new LoginForm();
        MainFrame.getInstance().getFrame().getContentPane().remove(MainFrame.getInstance().getPanel());
        loginForm.setVisible(true);
    }

    private void navigateToAddManager() {
        setVisible(false);
        AddUserPanel addUserPanel = new AddUnitManagerPanel(userFacade);
        MainFrame.getInstance().getFrame().getContentPane().remove(MainFrame.getInstance().getPanel());
        addUserPanel.setVisible(true);
        frame.setTitle("ثبت مدیر جدید");
        frame.pack();
    }
    private void navigateToAddEmployee() {
        setVisible(false);
        AddUserPanel addUserPanel = new AddEmployeePanel(userFacade);
        MainFrame.getInstance().getFrame().getContentPane().remove(MainFrame.getInstance().getPanel());
        addUserPanel.setVisible(true);
        frame.setTitle("ثبت کارمند جدید");
        frame.pack();
    }

    private void navigateToAddEvaluator(){
        setVisible(false);
        ShowCategories addEvaluatorPanel = new ShowCategories(metricFacade, userFacade);
        MainFrame.getInstance().getFrame().getContentPane().remove(MainFrame.getInstance().getPanel());
        addEvaluatorPanel.setVisible(true);
        frame.setTitle("انتخاب دسته ارزیابی");
    }
    private void navigateToAddEvaluation(){
        setVisible(false);
        AddEvaluationPanel addEvaluationPanel = new AddEvaluationPanel(metricFacade);
        MainFrame.getInstance().getFrame().getContentPane().remove(MainFrame.getInstance().getPanel());
        addEvaluationPanel.setVisible(true);
        frame.setTitle("افزودن معیار ارزیابی");
    }
    private void navigateToCategorize(){
        setVisible(false);
        Object[][] data = metricFacade.showAllMetrics();
        CategorizePanel categorizePanel = new CategorizePanel(data, metricFacade);
        MainFrame.getInstance().getFrame().getContentPane().remove(MainFrame.getInstance().getPanel());
        categorizePanel.setVisible(true);
        frame.setTitle("دسته‌بندی معیار");
    }
    private void navigateToRewardPunishment(){
        setVisible(false);
        ShowEvaluations showEvaluations = new ShowEvaluations(metricFacade);
        MainFrame.getInstance().getFrame().getContentPane().remove(MainFrame.getInstance().getPanel());
        showEvaluations.setVisible(true);
        frame.setTitle("تعیین روش تشویق و تنبیه");
    }

    private void createComponents() {
        JFrame frame = MainFrame.getInstance().getFrame();
        JMenuBar menuBar = frame.getJMenuBar();
        menuBar.removeAll();

        logoutMenuItem = new JMenuItem("خروج از سامانه");
        menuBar.add(logoutMenuItem);

        addEmployeeMenu = new JMenu("ثبت کارمند جدید");
        menuBar.add(addEmployeeMenu);

        addUnitManagerMenuItem = new JMenuItem("ثبت مدیر بخش جدید");
        addEmployeeMenu.add(addUnitManagerMenuItem);

        addEmployeeMenuItem = new JMenuItem("ثبت کارمند جدید");
        addEmployeeMenu.add(addEmployeeMenuItem);

        evaluationMenu = new JMenu("اقدامات مربوط به معیارها");
        menuBar.add(evaluationMenu);

        addEvaluationMenuItem = new JMenuItem("افزودن معیار ارزیابی");
        evaluationMenu.add(addEvaluationMenuItem);

        categorizeMenuItem = new JMenuItem("دسته بندی معیارها");
        evaluationMenu.add(categorizeMenuItem);

        setPunishmentRewardMenuItem = new JMenuItem("تعیین روش تشویق و تنبیه");
        evaluationMenu.add(setPunishmentRewardMenuItem);

        addEvaluatorToEvaluationMenuItem = new JMenuItem("افزودن ارزیاب به دسته ارزیابی");
        evaluationMenu.add(addEvaluatorToEvaluationMenuItem);

        menuBar.setVisible(true);
    }

    @Override
    public void setVisible(Boolean visible) {
        if (visible) {
            MainFrame.getInstance().getFrame().getContentPane().add(panel);
            MainFrame.getInstance().getFrame().validate();
            MainFrame.getInstance().setPanel(panel);
            MainFrame.getInstance().getFrame().pack();
        } else {
            MainFrame.getInstance().getFrame().getContentPane().remove(panel);
            MainFrame.getInstance().getFrame().repaint();
        }
    }

}
