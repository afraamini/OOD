package UI.ManagerUI;

import MetricDataManagement.Metric;
import MetricManagement.MetricFacade;
import UI.*;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ShowEvaluations implements Visibility {
    private JButton chooseBtn;
    private JButton cancelBtn;
    private JPanel panel;
    private MetricFacade metricFacade;
    private String targetMetric;

    private Object[][] data;
    private String[] columnNames;
    private JTable table;

    /**
     * Create the application.
     */
    public ShowEvaluations( MetricFacade metricFacade) {
        this.data = metricFacade.showAllMetrics();
        this.metricFacade = metricFacade;
        targetMetric = null;
        prepareData();
        createComponents();
        createEvents();
    }

    private void prepareData() {
        columnNames = new String[]{"ماهیت معیار", "دسته ارزیابی"};
    }
    private void onSelect(){
        if(targetMetric == null) new MainDialog(MessageType.NOT_COMPLETE,"معیاری انتخاب کنید");
        else{
            Metric metric = metricFacade.findMetric(targetMetric);
            if(metric.getPunishment()!=null && metric.getReward()!=null){
                new MainDialog(MessageType.NOT_COMPLETE,"اخطار!: روش تشویق و تنبیه قبلا ثبت شده است.");
            }
            setVisible(false);
            RewardPunishmentPanel rewardPunishmentPanel= new RewardPunishmentPanel(metric, metricFacade);
            rewardPunishmentPanel.setVisible(true);
        }
    }
    private void createEvents() {
        chooseBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onSelect();
            }
        });
        new CancelButton(cancelBtn,this);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = table.rowAtPoint(e.getPoint());
                if (data[row][1].equals("تعریف نشده"))
                    new MainDialog(MessageType.NOT_COMPLETE,"معیار درستی انتخاب نکرده اید");
                else
                    targetMetric = (String)data[row][0];
            }
        });
    }

    private void createComponents() {
        panel = new JPanel();
        GroupLayout groupLayout = new GroupLayout(panel);
        panel.setLayout(groupLayout);

        chooseBtn = new JButton("انتخاب معیار");
        chooseBtn.setForeground(new Color(0, 128, 0));

        cancelBtn = new JButton("لغو عملیات");
        cancelBtn.setForeground(new Color(255, 20, 147));

        JScrollPane scrollPane = new JScrollPane();
        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                        .addGroup(groupLayout.createSequentialGroup()
                                                .addGap(30)
                                                .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 481, GroupLayout.PREFERRED_SIZE))
                                        .addGroup(groupLayout.createSequentialGroup()
                                                .addGap(100)
                                                .addComponent(chooseBtn)
                                                .addGap(107)
                                                .addComponent(cancelBtn, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(33, Short.MAX_VALUE))
        );
        groupLayout.setVerticalGroup(
                groupLayout.createParallelGroup(Alignment.TRAILING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addContainerGap(23, Short.MAX_VALUE)
                                .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)
                                .addGap(18)
                                .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                        .addComponent(cancelBtn)
                                        .addComponent(chooseBtn))
                                .addGap(214))
        );

        table = new JTable(data, columnNames);
        scrollPane.setViewportView(table);
    }


    JPanel getPanel(){ return panel;}
    @Override
    public void setVisible(Boolean visible) {
        if(visible){
            MainFrame.getInstance().getFrame().getContentPane().add(panel);
            MainFrame.getInstance().getFrame().validate();
            MainFrame.getInstance().setPanel(panel);
            MainFrame.getInstance().getFrame().pack();
        }else{
            MainFrame.getInstance().getFrame().getContentPane().remove(panel);
        }
    }
}
